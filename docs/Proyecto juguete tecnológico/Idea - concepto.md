
# Idea - concepto

![](../images/idea1.jpg)

## Idea inicial

Jueguetes sostenibles que utilicen el mate (fruto no comestible tradicional), la programación electrónica y fabricación digital  como alternativa a los juguetes convencionales (plástico) y a la vez pueda servir como pieza que recoja la carga ancestral peruana y pueda customizarse con elementos característicos de  personajes reconocibles dentro de esta cultura.

## Concepto

***¿Qué es?(producto)***
Jueguete tecnológico sostenible modular que recoje tradición histórica y popular.  

***¿Para quien?(usuario)***
Se manejarian tres tipos de usuario de acuerdo al nivel de complejidad del juguete.

**Niños de 14 - 18 años** : Se manejaria una línea de producto más simple, que impulse el armado de sus juguetes por medio de expansiones (lineas temáticas similares al "señor cara de papa")

**Jóvenes adultos coleccionistas**: Se manejaria una línea de producto intermedia, teniendo una relación mas cercana a los ART TOYS, siendo piezas de colección y donde el valor recae en la exposición o su función ornamental (similar a lo realizado actualmente con los "POP" de FUNKO)

**Museos**: Constaria un línea avanzada, donde los juguetes tendrian una función de asistente social que pueda servir de guía para los visitantes y recurso lúdico temático.


***¿Por qué?(problemática)***

Actualemente se utiliza el MATE como elemento netamente decorativo o como envase para algunos alimentos como la bebida ancestral "chicha de Jora", sin embargo, cuenta con propiedades mecánicas de gran potencial, asi como una larga durabilidad al paso de los años, siendo utilizado por culturas pre-colombinas.  Es asi como este material se ve reducido a un papel ornamental, cuando podria revalorarse en otros escenarios.  

![](../images/mate2.jpg)

***¿Como?(funcionamiento)***

ELEMENTO 1: Se busca utilizar al MATE como pieza central del producto, estableciendo medidas tentativas tanto en diámetro como en alto para sistematizar en la medida de lo posible el trabajo con este material, ya que al ser un fruto no comestible natural, cada MATE por si solo es único.

ELEMENTO 2: Se busca dotar de elementos customizables y que se maneje por temáticas (historia, cultura popular,etc.). Para ello, se utilizará el modelado e impresión 3D para diseñar piezas que respondan a las temáticas establecidas, estas se vincularán con el meta por medio de PINES con diámetros estandarizados. Esto agilizará el proceso productivo y de prototipado.

ELEMENTO 3: Se busca implementar programación por medio de ARDUINO para darle funciones interactivas mas sofisticadas como reproducción de audio, sensores de movimiento o incluso movimientos mecánicos.  

***¿Para qué?(impacto)***

Existe la gran oportunidad de poder rescatar este recurso para darle visibilidad en formatos que permita darle una mayor trascendencia en formatos diferentes como la tecnología o juguetería. Asi mismo, puede generar una fuente importante de trabajo para los productores artesanos de MATE e incluso hacerlos participes del emprendimiento, revalorando su trabajo y exposición. Tambien, poder llevar el uso de la tecnología de impresión 3D a un sector, que en mi país, aún no está muy familiarizado.  

#Consigna

"Seleccionar 3 o más proyectos que se relacionen a tu idea de proyecto final. Explicar la elección en un slide (revisión en Proyecto innovador review).
Slide: Posible idea / referentes (ods?) / por qué?"

## Slide de ejercicio en clase (MIRO)
 ![](../images/slide.JPG)

## Referentes

**Tama**

Es una empresa peruana que se dedica al diseño de ART TOYS temáticos de la cultura nacional, utilizando como referente al TAMAL (Plato tipico peruano a base de maiz - choclo), el cual es customizado con elementos decorativos gráficos como físicos.

![](../images/Tama.JPG)

**Cozmo**

Es un pequeño dotado con una mente propia. Es un robot de la vida real como el que solo has visto en las películas, con una personalidad única que evoluciona a medida que pasas el rato. Te empujará para que juegues y te mantendrá sorprendido constantemente. Cozmo es tu cómplice en una loca cantidad de diversión.

*Curiosidad* Cozmo tiene una versión 2.0 llamada **Anki Vector**

![](../images/cozmo.JPG)
**[Amazon Cozmo](https://www.amazon.es/Anki-Juguete-Educativo-Controlado-Aplicaci%C3%B3n/dp/B0747LZTM8)**

**Todo Bambú**

Debido a que los juguetes son una parte muy importante para la formación y el desarrollo apropiado de los niños, los creadores de los juguetes de bambú decidieron extender su catálogo a una inmensa cantidad y variedad de juguetes tradicionales que además de ser hermosos, también llamen la atención de todos

![](../images/bambu 1.JPG)
