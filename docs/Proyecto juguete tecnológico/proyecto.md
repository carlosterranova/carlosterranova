#**JUGUETE TECNOLÓGICO**: ASISTENTE SOCIAL PARA LA GUÍA DE MUESTRAS MUSEOLÓGICAS


![](../images/01.7.jpg)


##***¿Qué hace?***
Es un asistente social que sirve de guía para las diferentes piezas de museo, el cual se mimetiza con la estética del espacio y mediante un recurso sonoro acompaña las descripciones de los objetos exhibidos. De esta forma permite volver mas interactiva la exposición asi como poder abordar un público invidente que puede recrear la exposición por medio del audio.

![](../images/museo-sipan.jpg)

##***¿Cómo llegaste a la idea?***

Por la zona geográfica donde nací (Lambayque / Perú), se vive la cultura ancestral del pais a flor de piel, ya que se hicieron muchos descubrimientos arqueológicos como el Señor de Sipán o la Huaca Rajada. Asi mismo, en la misma zona, se utilizaba un fruto no comestible (EL MATE) como material para la creación de obras artesanales desde la existencia de culturas nativas hasta nuestra fecha.
Por ello, al entender el potencial del MATE (larga durabilidad y buenas propiedades mecánicas para su trabajo) además de su relación cultural, decidí explorar su potencial como juguete o asistente social por medio de su intervención con procesos de fabricación digital y electrónica.

Finalmente, mi motivación es revalorar el material y valor cultural de cara a la sociedad.

![](../images/a21.jpg)


##***¿Quién lo ha hecho de antemano?***

No existe una referencia como tal de una intervención del MATE como elemento para la creación de juguetes tecnológicos o asistentes social. La referencia mas cercana se encuentra en las piezas decorativas elaboradas por artesanos de diferentes regiones del Perú.


##***¿Qué diseñaste?***
Para esta primera aproximación al juguete tecnológico o asistente social se diseñó los siguientes elementos:

- ***Case o base:*** Se diseñó una case que contiene los diferentes componentes electrónicos como sensor de proximidad, Arduino UNO, altavoz, lector de memoria microsd y luces LED. Dicho elemento está inspirado en las pirámides o huacas de la cultura Moche / Mochica.
Software: Fusion 360 - Arduino (Software nativo)
Procesos: Impresión 3D - programación

![](../images/1.png)


- ***Elementos temáticos para MATE:*** Se diseñaron los distintos elementos temáticos que van desde el tocado, nariguera, orejas y aretes inspirados en la cultura Moche / Mochica, específicamente del Señor de Sipán. Se pensaron dichos componentes para que se puedan acomplar con el mate por medio de pines tubulares y que puedan ser fabricados por medio de impresión 3D.
Software: Fusion 360
Procesos: Impresión 3D

![](../images/2.png)

- ***Audioguía*** Se guionizó un escrito referencial con indicaciones sencillas para simular su uso dentro del munseo . Posteriormente se grabó y guardó con el formato indicado para su reproducción dentro del juguete tecnológico.

![](../images/3.png)

##***¿Qué materiales y componentes se utilizaron?***

- ***MATE***
Nombre científico: Lagenaria siceraria.

Nombre colocial:  mate o calabaza en Perú.

Recibe los nombres comunes de mate, calabaza de peregrino, calabaza vinatera,​ capallu de Chile3​ y guada de Chile; guaje, bule, jícaro, porongo o acocote en México; choco​ en Colombia; y bangaño6​ o bangaña​ en Cuba.

En Argentina, Bolivia, Chile, Uruguay, Paraguay, y Brasil, el fruto seco y curado es el recipiente tradicional para el mate, a tal punto que la bebida toma su nombre por metonimia del término quechua para la calabaza: mati o mathi.

 En Perú existe una larga tradición en la fabricación del mate burilado. Mantuvo diversos estilos durante el Antiguo Perú y se transformó durante la Colonia. Este  fruto de corteza leñosa que constituye el soporte para la elaboración de los mates burilados.

 ![](../images/ab1.webp)

 - ***Arduino UNO***

Se utilizó este microcontrolador ya que es el dispositivo que hemos venido explorando durante la especialización. Asi mismo resultó mas fácil para el proceso de programación, ya que se encontraron referentes de fácil acceso en la web.  

![](../images/ab2.jpg)


 - ***Módulo lector microsd (MINI MP3 PLAYER)***

 Se buscaba un módulo que pueda ser trabajado con el microprocedador ARDUINO UNO que pueda ser compatible y a la vez permita la reproducción de sonido por medio de la grabación de guias tentativas para añadir otra experiencia durante los recorridos en museos.

 Se grabó en formato WAV. el cual es compatible con los requerimientos para la reproducción en su programación.

![](../images/CONEXIONES MODULO LECTOR SD.png)
![](../images/ab3.jpg)
[TUTORIAL](https://www.youtube.com/watch?v=JoU7lTGQ3iU&t=1s)

 - ***Módulo de proximidad HC - SR04 (SENSOR DE PROXIMIDAD)***

 Permite medir distancia mediante ultrasonido desde 2cm hasta 450cm, ideal para usar con Arduino o PIC, muy utilizado en Robots Exploradores.

 ![](../images/UL.jpg)


##***¿Qué partes y sistemas se fabricaron?***

Se fabricaron los siguientes componentes:

- *Base / case:*
Se fabricaron los siguientes componentes que conforman el case o contenedor de electrónica en el sistema:

-Tapa inferior

-Pirámide o contenedor general

-Soporte para altavoces

![](../images/base1.jpg)

- *Elementos temáticos para MATE* :

Se fabricaron los siguientes componentes que conforman el set para la personalización del MATE en el sistema:

Tocado, orejeras, nariguera

![](../images/pieza1.jpg)


##¿Qué procesos se utilizaron?

- *Fabricación Digital*
Se utilizó una impresora FLASHFORGE DREAMER con las siguientes características:

-Tecnologías: FFF (Fused Filament Fabrication)

-Precisión: En FFF +/- 0.05 mm por mm de construcción

-Temperatura máxima de extrucsor : 240℃ (464℉)

-Velocidad de impresión: 30 -100mm/s

-Temperatura máxima de plataforma: 120℃ (248℉)

-Compatibilidad de filamento: PLA, TPU95A, ABS, PETG

-Material de impresión utilizado: PLA

- *Proceso de fabricación tradicional*

Se recurrió a la intervención del MATE de su estado natural por medio de la generación de orificios generados por la utilización de brocas de distinto diámetro dependiendo del elemento con el cual interactua.

Únion con Base

Tocado

Nariguera

Orejeras


##¿Qué preguntas se respondieron?

- ¿Qué funcionó? ¿Qué no?

En líneas generales funcionaron muy bien todos los componentes luego de varias pruebas y error, sin embargo vamos a detallar por elemento las lecciones aprendidas:

###IMPRESIÓN 3D GENERAL:
- Utilicé la impresora 3D detallada en apartados anteriores, teniendo multiples complicaciones iniciales para poder tener piezas que no se levanten del área de impresión.
- Procedí a aumentar la temperatura de la mesa de impresión y de los estrucsores.
- Reduje la velocidad de impresión para evitar  inconvenientes.

Resultado: **PROBLEMA PERSISTÍA**

![](../images/levan.jpg)
[video de impresión](https://photos.app.goo.gl/PhTrKnheGs9iUfdn8)

####SOLUCIÓN:
- Busqué tutoriales y hacks en internet (youtube)
- Encontré alternativas como a la cama de vidrio, recubrirla con cinta texturada.
- Encontré la opción de añadir pegamento sobre la cinta texturada

Resultado: **PROBLEMA RESUELTO**

![](../images/resul.jpg)
[video de impresión](https://photos.app.goo.gl/5jrWahW9oNTAsovx6)


###PROGRAMACIÓN:

- Se utilizó las librerias necesarias para el funcionamiento de 3 LEDs, reproductor de sonido y sensor de proximida.
- Se modificaron los valores a partir de la necesidad de cada una de las funciones.

####PROCESO

![](../images/program1.jpg)
![](../images/program2.jpg)

Resultado: **SENSOR CAPTABA CUALQUIER MOVIMIENTO A 200 CM DE DISTANCIA, ACTIVANDO SU PROGRAMACIÓN CONSTANTEMENTE**

####SOLUCIÓN

- Se modificaron los valores del sensor de proximidad, reduciendo su alcance de 200 CM a 50 CM para que se puda funcionar correctamente.

Resultado: **SENSOR CAPTA MOVIMIENTOs A 50 CM DE DISTANCIA, ACTIVANDO SU PROGRAMACIÓN OPORTUNAMENTE**

[video de la programación](https://photos.app.goo.gl/BZ6csCMFSCE5LL1r7)

- ¿Cuáles son los pasos a seguir?

1. Planteamiento de acabados finales para el prototipo funcional.
2. Upgrate de componentes electrónicos para mejora la calidad de sonido.
3. Desarrollo de esquema de costos de esta versión.
4. Exploración de otros procesos de manufactura para brindarle otros acabados.
5. Escalar al estatus de emprendimiento.

##RESULTADO FINAL

![](../images/PRO1.jpg)
![](../images/PRO2.jpg)
![](../images/IMG_0922.JPG)
![](../images/IMG_0923.JPG)
[ACTIVACIÓN](https://photos.app.goo.gl/FfNLHw8WzLb6ddPCA)

#SLIDE DE PRESENTACIÓN
![](../images/SF.png)
[PDF](https://drive.google.com/file/d/1bfLyt2deqNwheY6xibXSybEkbgajETpJ/view?usp=sharing)

#VIDEO PROYECTO
![](../images/MATEGO.png)
[MATEGO VIDEO](https://drive.google.com/file/d/1zwOLOtFrJx1G0F0vlRFY6vZs6Ceknsn5/view?usp=sharing)

#ARCHIVO EDITABLE CAD
[MATEGO P1](https://drive.google.com/file/d/1_43SQe0L__VJQbUXKbW5WzcH1a8nttr1/view?usp=sharing)

#ARCHIVO RENDER BIP
[KEYSHOT](https://drive.google.com/file/d/1_43SQe0L__VJQbUXKbW5WzcH1a8nttr1/view?usp=sharing)

#ARCHIVO DE PROGRAMACIÓN
[ARDUINO.INO](https://drive.google.com/file/d/1cLukBc3r0YaR9zwbee3ZlzwQF2zMEAcL/view?usp=sharing)
