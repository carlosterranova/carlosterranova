#Inicio

##**Bienvenidos**


![](../images/digi.jpg)

###Este es el  repositorio o bitácora , en el cual estaré actualizando todo mi proceso de aprendizaje en la Especialización en Fabricación Digital y Innovación Abierta **(EFDIA)**.

##¿Pero, que es la EFDIA?

El Programa apunta a desarrollar profesionales capacitados en generar propuestas innovadoras y adaptadas a las necesidades de diferentes áreas de la producción y el diseño, en nuestro país y la región, haciendo uso de metodologías de innovación abierta asistidas por tecnologías de fabricación digital.

La malla curricular se compone de Módulos Técnicos (MT), Módulos de Profundización (MP), Módulos de Innovación (MI), un Taller Integrador presencial (TIp) y el Proyecto Final (PF). Se contará con una red de apoyo de laboratorios de Fabricación Digital para las instancias presenciales.
En la Especialización se exige la realización de un Proyecto final a realizar de forma grupal o individual, aplicado en un área específica a elección que deberá estar relacionada a un problema real que pueda solucionarse con los métodos y herramientas adquiridos en la especialización.

La Especialización en Fabricación Digital e Innovación te permitirá formarte con  referentes y expertos de UTEC y del Fab Lab Barcelona en el  Instituto de Arquitectura Avanzada de Cataluña  (IAAC), quienes codirigen el programa y realizan la curaduría. Fab Lab Barcelona es el primer laboratorio  de fabricación digital de la Unión Europea creado en  2007 a partir del Center for Bits and Atoms (CBA)  del Massachusetts Institute of Technology (MIT)

![](../images/01.2.JPG)

![](../images/01.3.JPG)

##Clases en EDU

Las clases se encontrarán alojadas en la plataforma **EDU**, donde se podrá profundizar las sesiones realizadas en las masterclass, Laboratorios y sesiones de profundización.

![](../images/01.5.JPG)

**[EFDIA](https://utec.edu.uy/es/educacion/posgrado/especializacion-en-fabricacion-digital-e-innovacion/)**

**[Cursos de EDU](https://edu2.utec.edu.uy/dashboard)**

**[Repositorio de los participantes](https://efdia20212.gitlab.io/efdia2021/)**

**[Clases grabadas](https://drive.google.com/drive/folders/1OHahaXaPZam9gN3H7PjZZRDKHRrudnwy)**


##Expectativa

1.Poder tener mayor autonomía a la hora de proponer, desarrollar y ejecutar proyectos tecnologícos vinculados a impresión 3D y programación.

2.Extender los conocimientos aprendidos en la especialización a mi trabajo en el sector público y las difererentes regiones de mi pais.

3.Enseñarle a niños sobre el mundo de fabricación digital.

4.Desarrollar mis propios juguetes tecnológicos.

5.Cumplir el sueño de niño de convertirme en inventor.

![](../images/01.4.JPG)
