#**MT 07** INTERNET OF THINGS

![](../images/ardui.JPG)

### NETWORKING AND COMUNICATION
El uso de redes y protocolos de comunicación se basa en la idea de distribuir y conectar sistemas por motivos de:

**ubicación:**  no es posible realizar todas las tareas de procesado/cálculo o gestión de hardware en el mismo sitio
**paralelismo:** poder realizar cosas en paralelo
**modularidad:** hacer desarrollos en módulos, cada uno con una función muy concreta, fácilmente escalable y más fácilmente reparable
interferencia: evitar que los sistemas interfieran entre ellos

####PROTOCOLO vs RED

Una red es un conjunto de ordenadores conectados entre sí a través de líneas de comunicación.

Los protocolos son conjuntos de normas para formatos de mensaje y procedimientos que permiten a las máquinas y los programas de aplicación intercambiar información.

####TIPO
Las redes se pueden clasificar de muchas maneras:
en función del medio de transmisión: cable, óptico, radio frecuencia
![](../images/A4.JPG)

En función de la topología
![](../images/A5.JPG)

En función de su rango
![](../images/A6.JPG)

####CASO PRÁCTICO

![](../images/A7.JPG)
[CLASE](https://hackmd.io/@vico/ByGtYSbAt)

## CONSIGNA

MT07 - Desafío
***LEER UN INPUT (Botón, sensor, potenciómetro, etc...)
Y PUBLICARLO POR MQTT Y SUBSCRIBIRSE A LA PUBLICACIÓN DEL
INPUT DE UN COMPAÑERO Y USARLO PARA CONTROLAR
ACTUADOR (Servo, LED, etc...)***

### PROCESO

Mi grupo de trabajo está conformado por :
Robert Gomez
Gabriela Mejia

![](../images/0.001.JPG)

Iniciamos el trabajo definiendo los roles dentro del desafio, siendo Robert quien tomaria la posición de emisor, mientras que Gaby y yo la de receptores.

![](../images/iot1.jfif)
Procedimos a armar nuestros respectivos arduinos de acuerdo a las indicaciones de clase.

![](../images/iot4.jfif)

[![Paso I](../images/iot2.JPG)](https://drive.google.com/drive/folders/1G5OyXHBIZIxFI6tbJ7fcl-HxlIlx29gX)

Asi mismo, revisar si estaban funcionando correctamente

[![Paso II](../images/iot3.JPG)](https://drive.google.com/drive/folders/1OggMUede889m7ccYGD1FsKJpCv8gT61S)

Tambien, corroborar si el LED podia encender

[![Paso III](../images/iot5.JPG)](https://drive.google.com/drive/folders/1_idcK-HeXYkvcJL9UWDsp-mmQKKLGaQJ)

Se presentaron problemas de transmisión y recepción, en el caso de Robert (transmisión) su red no le permitía generarla por protocolos internos , miemtras que en mi caso, primero no habia instalado la libreria correctamente y posteriormente, se presentaban errores no identificables en los puertos.

![](../images/iot6.JPG)
![](../images/iot7.JPG)


[![Paso IV](../images/iot9.JPG)](https://drive.google.com/drive/folders/1UPmBUPzpmSzQbqn7rysrG25iT01rE229)


Por su cuenta, Robert empezó a realizar pruebas para identificar el error en la transmisión
![](../images/iot9.jfif)
