#**MT 02** Diseño asistido por computadora

![](../images/raster 2.jpg)
En este módulo de trabajo se aprendió el origen y aplicación del diseño asistido por computadora CAD.


##**CAD**

El software de diseño asistido por ordenador, mayormente conocido por las siglas CAD que provienen del inglés Computer-Aided Design, es un software para crear y editar modelos bidimensionales y tridimensionales de objetos físicos. Aunque también puede encontrarse como CADD, Computer-Aided Design & Drafting.

##***¿En que consiste el diseño asistido por computadora?***

El diseño asistido por ordenador (CAD) consiste en el uso de programas de ordenador para crear, modificar, analizar y documentar representaciones gráficas bidimensionales o tridimensionales (2D o 3D) de objetos físicos como una alternativa a los borradores manuales y a los prototipos de producto. El CAD se utiliza mucho en los efectos especiales en los medios y en la animación por ordenador, así como en el diseño industrial y de productos

##**vector**

Una imagen vectorial es una imagen digital formada por objetos geométricos dependientes (segmentos, polígonos, arcos, muros, etc.), cada uno de ellos definido por atributos matemáticos de forma, de posición, etc. ... De la misma forma, permiten mover, estirar y retorcer imágenes de manera relativamente sencilla

##**Raster**

El raster es un tipo de imagen que usa una cuadrícula rectangular de colores para representar las imágenes. Cada punto de la cuadrícula es un píxel. Las imágenes rasterizadas son bitmaps o mapas de bits que pueden tener distintos formatos de archivo.

A cada píxel se le asigna una ubicación y un valor de color específicos. Al trabajar con imágenes de mapa de bits, se editan los píxeles en lugar de los objetos o las formas.

Las imágenes de mapa de bits son el medio electrónico más usado para las imágenes de tono continuo, como fotografías o pinturas digitales, puesto que pueden representar degradados sutiles de sombras y color.

Las calidad de las imágenes rasterizadas se mide por su altura, anchura y profundidad de color.

##***¿En que se diferencia Vector y Raster?***

![](../images/raster3.jpg)

-La principal ventaja del modelo vectorial es la gran capacidad de compactar información utilizando el menor volumen de datos posible del SIG.

-En cuanto a la precisión de ambos modelos, los archivos vectoriales son más precisos que los ráster cuando se calculan superficies y distancias.

-Los modelos vectoriales permiten tener límites más precisos al tratarse de líneas y puntos de fácil definición  y distribución, favoreciendo las relaciones de vecindad entre elementos y haciendo a estos archivos los más óptimos cuando se quiere realizar un análisis entre unidades espaciales. Por el contrario , los modelos ráster, presentan límites basados en el propio tamaño de píxel y tienen ciertas dificultades para desarrollar análisis espaciales.
-Desde un punto de vista estructural, el modelo ráster es un modelo sencillo y básico, pero poco compacto y tiene bastantes dificultadas para representar información cuando se tienen archivos muy pesados.

-Los modelos ráster pueden simular tridimensionalmente la realidad de forma más fiable, por el contrario los archivos vectoriales tienen un carácter plano y no están capacitados para ser representados de la misma forma en el espacio.

-A nivel comercial los archivos vectoriales son más utilizados y compartidos, mieentras que los archivos ráster son más difíciles de generar y conseguir, por lo que presentan un coste económico más elevado.

-Graficamente los modelos ráster representan mejor la realidad por lo que las salidas gráficas bajo un modelo ráster permiten una representación de la realidad algo más realista que los modelos vectoriales.

-Cuando se trata de asignar atributos cuantitativos o cualitativos, son los modelos vectoriales los que presentan mayor facilidad de edición frente a los archivos ráster , sin embargo son los modelos ráster los que admiten mejor la incorparación de datos desde el inicio de la cración del archivo cuando se trata de imágenes satelitales.

-Los archivos vectoriales tiene mayor facilidad a la hora de desarrollar reglas y condiciones topológicas, respecto a los archivos ráster , pero por el contrario generan con más facilidad problemas topología (solapamientos entre elementos de la misma capa).


##**Softwares a explorar**  

**[GIMP](https://www.gimp.org/)**
**[Krita](https://krita.org/en/)**
**[Pixlr](https://pixlr.com/)**
**[Inkscape](https://inkscape.org/)**
**[Fusion 360](https://latinoamerica.autodesk.com/products/fusion-360/overview?mktvar002=4424517|SEM|13037407432|120424532325|aud-1166343329429:kwd-11029869505&ef_id=CjwKCAjwk6-LBhBZEiwAOUUDp_BML0e1_UnQlbYUtQLvXy_hMctBaH5OipX3o8PCLDEdMx5nPfDCKxoCimcQAvD_BwE:G:s&s_kwcid=AL!11172!3!549334725048!e!!g!!fusion%20360!13037407432!120424532325&mkwid=s|pcrid|549334725048|pkw|fusion%20360|pmt|e|pdv|c|slid||pgrid|120424532325|ptaid|aud-1166343329429:kwd-11029869505|pid|&utm_medium=cpc&utm_source=google&utm_campaign=GGL_DEC_AutoCAD_AMER_MX_eComm_SEM_BR_New_EX_ADSK_3360535_General&utm_term=fusion%20360&utm_content=s|pcrid|549334725048|pkw|fusion%20360|pmt|e|pdv|c|slid||pgrid|120424532325|ptaid|aud-1166343329429:kwd-11029869505|&gclid=CjwKCAjwk6-LBhBZEiwAOUUDp_BML0e1_UnQlbYUtQLvXy_hMctBaH5OipX3o8PCLDEdMx5nPfDCKxoCimcQAvD_BwE)**

##**Consigna de MT02**

Diseñar (raster, vector, 2D, render, anime, simule, ...) un montaje de su posible idea de proyecto final, utilizando herramientas raster y un isotipo (vector) de su proyecto suiguiendo la lógica gráfica de Utec (https://red.utec.edu.uy/recursos/).

Te sugerimos que experimentes con más de un software o elijas un software que no hayas usado antes.

Documentar tu proceso y publicar en tu repositorio de gitlab.

##**Proceso**

###**Montaje utilizando herramientas raster**

**Photoshop**
En un principio, utilicé este software para hacerme una idea de lo que estaba buscando o mejor dicho, crear un collage que me sirva de referencia para plantear el posterior montaje dentro del software en linea PIXLR.

![](../images/prueba 1.jpeg)

Utilizar este software no me resultó problemático, puesto que es uno de los programas con los que mas he trabajado, teniendo como resultado la siguiente composición.

![](../images/raster.jpg)

**[Archivo photoshop](https://drive.google.com/drive/u/0/folders/11gDgNLAvkegpNaQsqabV3TBeAt7jsPaK)**

**PIXLR**

Nunca habia utiliza este software en linea, siendo una grata opción para utilizar en espacios que no cuenten con Adobe suit.

AL inicio, me parecio similar a la interfaz de Photoshop pero al empezar a hacer el reconocimiento de herramientas, sentí que era un poco limitado, ya que queria utilizar máscaras para suavizar la superposición de imagenes y no encontraba la opción.  
![](../images/mt02.JPG)

Invitiendo un poco mas de tiempo, pude encontrar comandos similares dentro de los íconos ubicados a la izquiera, como la opción tampón o clonar que me resultaron de suma utilidad para poder realizar el montaje.
![](../images/mt02-1,JPG)

Las herramientas de recorte me resultaron un poco dificiles para adaptarme rápido, pero pude arreglarmelas para calar las imagenes que necesitaba para hacer mi composición. Siendo este el resultado:

![](../images/mt2.png)

CONCLUSIÓN

PIXLR es una muy buena alternativa a photoshop, sobre todo en aquellos escenarios donde no se cuenta con el paquete de adobe. Asi mismo, puede ser una opción para enseñar en colegios o instituciones públicas.

**[PIXLR RUTA DE COMPOSICIÓN](https://pixlr.com/es/x/#editor)**


###**Isotipo**

**INKSCAPE**

Antes de la clase, no sabia de la existencia de este programa, el cual, a primera vista me resulta mas similar al Corel Draw que a Illustrator.  Particularmente, me parece un poco pesada la interfaz pero es cuestión de familiarizarse.

*recursos*
Primero, es necesario familiarizarse tanto con el plano de trabajo como con los comandos o ShortCuts
que se usarán constantemente, para poder agilizar el flujo de trabajo.
Al inicio tuve problemas para hacer el zoom, por lo que busqué  recursos que me permitan aprender estos comandos. ( Se puede hacer zoom in o zoom on con presionando - o + respectivamente)

![](../images/comandos ink.JPG)

![](../images/ink 2.JPG)

**[ShortCuts de Inkscape](https://inkscape.org/es-mx/doc/keys.html?switchlang=es-mx)**

Se procedió a diagramar los isotipos a partir de conceptualizar primero mi proyecto final. En el cual el elemento MATE es relevante y distintivo, por lo que se sintetiza, además se añade el elemento robótico como referencia a juguete tecnológico.

![](../images/INK-3.png)

Todos estos elementos tenian que poder generar una propuesta alineada a la identidad gráfica de UTEC, por lo que se adoptan la estructura lineal descontinuada como característica principal.

![](../images/INK4.jpg)

Realicé algunas propuestas, siendo la de la ubicada a la izquierda la que creo que ajusta mas al concepto que estoy buscando

![](../images/logo.JPG)


![](../images/logo2.JPG)

Se adjunta tambien el link para descargar el archivo nativo de inkscape: SVG de Inkscape

**[Isotipo Juguetes tecnológico](https://drive.google.com/drive/u/0/folders/1zBwTXhBE7jGPilUqW-DSdogu207qpO3Y)**
