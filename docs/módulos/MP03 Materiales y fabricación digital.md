---

---
#**MP 03** MATERIALES Y FABRICACIÓN DIGITAL

![](../images/ec2.jpg)

Se realizaron clases maestras con especialistas vinculados a distintas aproximaciones en el manejo de biomateriales para la innovación en esta rama.

##SESIÓN 1 :
MATERIALES EN CONTEXTO PARA UNA ECONOMÍA CIRCULAR
EXPOSITORA: LAURA FREIXAS

PRINCIPIOS:

1. Eliminar los residuos y la contaminación.

2. Hacer circular los productos y materiales (a su máximo valor).

3. Regenerar la naturaleza.

#### CONCEPTOS A TENER EN CUENTA
- Motivación para el  repensamiento
- Profesionales con la motivación para la concepción de convebir nuevos materiales y productos
conscientes.
- Manejo de productos con la visión de una sencilla reparación y facilidad para el usuario.

###ECONOMÍA CIRCULAR

La economía circular es un modelo de producción y consumo que implica compartir, alquilar, reutilizar, reparar, renovar y reciclar materiales y productos existentes todas las veces que sea posible para crear un valor añadido. De esta forma, el ciclo de vida de los productos se extiende.

####¿Cuáles son los beneficios?


Medidas como la prevención de residuos, el diseño ecológico y la reutilización podrían ahorrar dinero a las empresas de la UE mientras se reduce el total anual de emisiones de gases de efecto invernadero. Actualmente, la producción de los materiales que usamos diariamente son responsables del 45% de las emisiones de CO2.


Avanzar hacia una economía más circular podría generar beneficios como reducir la presión sobre el medioambiente, mejorar la seguridad de suministro de materias primas, estimular la competitividad, la innovación, el crecimiento económico (un 0,5% adicional del PIB) y el empleo (se crearían unos 700.000 trabajos solo en la UE de cara a 2030).


También puede proporcionar a los consumidores productos más duraderos e innovadores que brinden ahorros monetarios y una mayor calidad de vida, por ejemplo, si los teléfonos móviles fuesen más fáciles de desmontar el coste de volverlo a fabricar podría reducirse a la mitad.

![](../images/EC1.jpg)



##SESIÓN 2 :
FAB TEXTILES / FABRICACIÓN DIGITAL EN LA MODA
EXPOSITORA: ANASTACIA PISTOFIDOU

BIODISEÑO:

1. participación activa del diseñador para identificar la procedencia de los materiales, condiciones de uso y lugar final de vida util.
2. Evitar el uso de colores nocivos para el medio ambiente.
3. Utilizar tintes naturales que procedan de elementos de la naturaleza.
4. investigar y explorar nuevas alternativas de materiales que puedan compararse en características técnicas con los ya exitentes.


![](../images/fab1.png)
