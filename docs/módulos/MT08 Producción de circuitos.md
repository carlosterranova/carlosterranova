#**MT 08** Producción de circuitos

![](../images/B0.jpg)

### PCB
PCB son las siglas de Placa de Circuito Impreso, pero utilizamos las siglas en inglés (Printed Circuit Board) para no confundirla por ejemplo con las ranuras PCI de nuestro PC.

Pues una PCB básicamente es un soporte físico en donde se instalan componentes electrónicos y eléctricos y se interconectan entre ellos. Estos componentes pueden ser, chips, condensadores, diodos, resistencias, conectores, etc. Si echas un vistazo a un ordenador por dentro, verás que hay múltiples placas planas con un montón de componentes pegados a ella, se trata de una placa base y está compuesta por una PCB y los componentes que hemos citado

Para conectar cada elemento en una PCB utilizamos una serie de pistas conductoras de cobre extremadamente finas y que general un carril, conductor, como si de un cable se tratase. En los circuitos más sencillos, solamente tenemos pistas conductoras en una cara o las dos visibles de la PCB, pero en otros más completos tenemos pistas eléctricas e incluso componentes apilados en múltiples capas de ellas.

El soporte principal para estas pistas y componentes es una combinación de fibra de vidrio reforzada con materiales cerámicos, resinas, plástico y otros elementos no conductores. Aunque actualmente se están utilizado componentes como celuloide y pistas de pintura conductora para fabricar PCB flexibles.

![](../images/B1.JPG)


### ¿QUÉ HAY EN UNA PCB?

Los circuitos impresos están compuestos por una serie de capas conductoras, al menos los más complejos. Cada una de estas capas conductoras está separada mediante un material aislante que se llama sustrato. Para conectar pistas de distintas capas se utilizan orificios llamados vías que pueden atravesar completamente la PCB o solamente llegar hasta una determinada profundidad.

![](../images/B2.JPG)

l sustrato puede ser de distintas composiciones, pero siempre de materiales no conductores para que cada una de las pistas eléctricas lleven su propia señal y voltaje. El más utilizado actualmente se llama Pértinax que básicamente es un papel cubierto de resina, muy fácil de manejar y de mecanizar. Pero en los equipos de altas prestaciones se utiliza un compuesto llamado FR-4 es que un material de fibra de vidrio cubierto de resina resistente al fuego.

Los componentes electrónicos por su parte, irán casi siempre en la zona externa de las PCB, e instalados en ambas caras, para aprovechar al máximo la extensión de ellas. Antes de crear las pistas eléctricas, las distintas capas de la PCB solamente están formadas por el sustrato y unas láminas muy fina de cobre u otro material conductor, y será mediante una máquina similar a una impresora como se crearán éstas y a través de un proceso bastante largo y complejo.

### TIPOS DE PCB
![](../images/B3.JPG)
![](../images/B4.JPG)
![](../images/B5.JPG)
![](../images/B6.JPG)
![](../images/B7.JPG)
![](../images/B8.JPG)
![](../images/B9.JPG)
![](../images/B10.JPG)

###FABRICACIÓN

Existen distintos métodos a la hora de producir PCB’s. A continuación, describimos algunas de las técnicas que existen en el mercado:

– Impresión serigráfica. Una opción en la que se utilizan tintas resistentes al grabado y a la protección de la capa de cobre.

– Fotograbado. Una técnica que se utiliza el grabado químico para eliminar del sustrato la capa de cobre.

– Fresado de circuitos impresos. Una metodología mucho más mecánica que permite eliminar con la fresadora el cobre del sustrato.

– Utilización de material termosensible. Esta técnica permite crear un circuito mediante la impresión de calor, de tal forma que la placa de cobre adquiere el material concreto.

-Milling. Se necesita accesorio, alta calidad ,cambio de herramienta necesario, rotura de fresas

-Fiber Laser. Alta calidad con alta velocidad, sin cambio de herramienta, defectos a escala microscopica, seteado delicado.

-Vinyl cut. Para realizar placas flexibles, durabilidad depende del sustrato, dificil de soldar

![](../images/B11.JPG)
![](../images/B12.JPG)


###SOLDADURA

La soldadura es el método para colocar uno o más componentes electrónicos en la PCB utilizando un soldador. Por ello, la soldadura de PCB también se denomina soldado de PCB. La soldadura se funde y fija los componentes electrónicos en su lugar. El punto de fusión del metal de la soldadura es menor que el de los componentes y el PCB.

**Soldadura de Plata**
Es un método impecable, útil para la fabricación de pequeños elementos, herramientas elaboradas y para hacer mantenimiento irregular. Deberás comprar una aleación de plata, que actuará como metal de relleno de huecos. Sin embargo, la soldadura de plata no se recomienda para el relleno de huecos. Sugeriremos que utilices un fundente diferente para obtener resultados precisos de la soldadura de plata.

**Soldadura Fuerte**
En la soldadura Fuerte, dos componentes se conectan mediante la creación de un relleno metálico líquido. Esta masilla seguirá el recipiente y recorrerá las uniones. A continuación, se enfriará para proporcionar una unión sólida a los componentes electrónicos. El magnetismo atómico y la difusión son los procesos responsables de esta unión sólida. Verás que este tipo de soldadura hace una unión muy fuerte. El metal de latón se utiliza sobre todo para rellenar los huecos. La figura 2 ilustra la vista cercana del elemento de cobre de soldadura fuerte.

**Soldadura Blanda**
La soldadura blanda es la técnica utilizada para colocar piezas compuestas muy pequeñas que tienen puntos de fusión bajos. Las piezas compuestas se habrían agrietado durante el proceso de soldadura. ¿Adivina por qué? Porque la soldadura se realiza a altas temperaturas. Por lo tanto, en este caso, tendrás que conseguir una aleación de estaño para el metal de relleno de huecos. El punto de fusión del metal de relleno del hueco no debe ser menor de 752◦F. Si buscas una recomendación de fuente de calor, te sugeriremos que compres un soplete de gas.

No te preocupes si no estás familiarizado con algunos términos de soldadura, como fundente, hierro, etc. En el próximo capítulo, explicaremos estos términos detalladamente. Además, te daremos algunos consejos para el proceso de soldadura.

### SOFTWARE

![](../images/B13.JPG)
![](../images/B14.JPG)
![](../images/B15.JPG)
![](../images/B16.JPG)

### CONSIGNA

Reproduce la placa de ejemplo en Kicad
Extra: Diseñar una placa que te permita conectar un sensor a un arduino sin
necesidad de un breadboard (diseña tu propio shield)

###PROCESO

![](../images/PCB1.png)

![](../images/PCB2.png)

![](../images/PCB3.png)

![](../images/PCB4.png)

![](../images/PCB5.png)

![](../images/PCB6.png)

![](../images/PCB7.png)

![](../images/PCB8.png)

![](../images/PCB9.png)

![](../images/PCB10.png)

![](../images/PCB11.png)

![](../images/PCB13.png)

![](../images/PCB14.png)

#ARCHIVOS EDITABLES

[EDITABLES KICAD](https://drive.google.com/drive/u/0/folders/1tXUc8po0A9516d7rp4D1yqYWhbV6sImr)
