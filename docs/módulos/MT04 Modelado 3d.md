#**MT 04** Modelado 3D


**¿QUE ES EL MODELADO 3D?**

![](../images/ts.jpeg)

El modelado 3D consiste en utilizar software para crear una representación matemática de un objeto o forma tridimensional. El objeto creado se denomina modelo 3D y se utiliza en distintas industrias.

Las industrias del cine, la televisión, los videojuegos, la arquitectura, la construcción, el desarrollo de productos, la ciencia y la medicina utilizan modelos 3D para visualizar, simular y renderizar diseños gráficos.

Fuente: Autodesk.

**NOTA CURIOSA:**

Autodesk brinda tutoriales gratuitos de la gran mayoria de sus productos
**[Autodesk Design Academy](https://knowledge.autodesk.com/support/maya)**

Pixar en conjunto con Khan Academy brinda un programa completo de formación en modelado y animación 3D
**[Pixar in a Box](https://es.khanacademy.org/computing/pixar#table-of-contents)**

**APLICACIONES DEL MODELADO 3D**

El modelado 3D tiene numerosas aplicaciones en la vida real, entre las que se encuentran las siguientes:

ARQUITECTURA: para visualizar una construcción antes de iniciar las obras y así poder detectar posibles errores o incongruencias.

INGENIERÍA: facilita la detección de fallos en los proyectos, permite controlar el peso y densidad de las piezas y posibilita la interacción y colaboración entre ingenieros, arquitectos y aparejadores.

DISEÑO DE PRODUCTOS: permite simular productos en tres dimensiones antes de su fabricación de manera que se pueda vender o conseguir financiación para lanzarlo al mercado.

IMPRESORAS 3D: este tipo de tecnología que tan de moda se ha puesto últimamente funciona con programas de diseño en 3D.

CINE: las películas de animación como las de Pixar son posibles gracias al modelado en 3D. Aunque cada año podemos ver muchas de ellas, la primera fue Toy Story en 1995.

VIDEOJUEGOS: consiguen crear personajes que realizan movimientos en el ángulo que el jugador quiera gracias a este tipo de tecnología.

![](../images/mesh.jpg)

**¿EN QUE SE DIFERENCIA  NURBS DE MESH ?**

**NURBS:**

Se calculan mediante una ecuación matemática, generando un paralelismo con los vectores en 2d.

Las superficies están definidas por curvas, las cuales son influenciadas por la ponderación del control de puntos. La curva sigue (pero no necesariamente interpola) los puntos, además de incrementar el peso de un punto va a enviar la curva más cercana a este.

Los tipos de curva incluyen nonuniform rational B-spline (NURBS), chavetas, parches y geometric primitives.

**MESH:**

Son puntos en un espacio 3D, llamados vértices, están conectados para formar un malla poligonal. La gran mayoría de los modelos 3D hoy en día están construidos como modelos de textura poligonal, porque son flexibles y porque las computadoras pueden renderizarlos muy rápido. Sin embargo, los polígonos son planos y solamente se pueden aproximar a superficies curvas usando varios polígonos.

**SOFTWARES A EXPLORAR**  

**[FLATLAB](http://flatfab.com/)**
**[Grasshopper Rhino](https://www.rhino3d.com/6/new/grasshopper/)**
**[Tinkercad](https://www.tinkercad.com/)**
**[onshape](https://www.onshape.com/en/)**
**[Fusion 360](https://latinoamerica.autodesk.com/products/fusion-360/overview?mktvar002=4424517|SEM|13037407432|120424532325|aud-1166343329429:kwd-11029869505&ef_id=CjwKCAjwk6-LBhBZEiwAOUUDp_BML0e1_UnQlbYUtQLvXy_hMctBaH5OipX3o8PCLDEdMx5nPfDCKxoCimcQAvD_BwE:G:s&s_kwcid=AL!11172!3!549334725048!e!!g!!fusion%20360!13037407432!120424532325&mkwid=s|pcrid|549334725048|pkw|fusion%20360|pmt|e|pdv|c|slid||pgrid|120424532325|ptaid|aud-1166343329429:kwd-11029869505|pid|&utm_medium=cpc&utm_source=google&utm_campaign=GGL_DEC_AutoCAD_AMER_MX_eComm_SEM_BR_New_EX_ADSK_3360535_General&utm_term=fusion%20360&utm_content=s|pcrid|549334725048|pkw|fusion%20360|pmt|e|pdv|c|slid||pgrid|120424532325|ptaid|aud-1166343329429:kwd-11029869505|&gclid=CjwKCAjwk6-LBhBZEiwAOUUDp_BML0e1_UnQlbYUtQLvXy_hMctBaH5OipX3o8PCLDEdMx5nPfDCKxoCimcQAvD_BwE)**
**[Illustrator](https://www.adobe.com/la/products/illustrator.html?sdid=KQPQL&mv=search&ef_id=CjwKCAjwn8SLBhAyEiwAHNTJbU_iyuN5znV4d9k7FUdf0W7tfQLbrsiUJGH7eJZonIq53uIgZZcfPRoCby4QAvD_BwE:G:s&s_kwcid=AL!3085!3!442303212642!e!!g!!illustrator!9499870682!97813414318&gclid=CjwKCAjwn8SLBhAyEiwAHNTJbU_iyuN5znV4d9k7FUdf0W7tfQLbrsiUJGH7eJZonIq53uIgZZcfPRoCby4QAvD_BwE)**

**CONSIGNA DE MT03**

"Diseña y modela en 3D un elemento que sea de utilidad para tu proyecto final en el software paramétrico FUSION 360. Documenta los distintos pasos del proceso en tu página web.

NOTA: Si bien este modulo no implica directamente la producción de una pieza, el modelo debe poder imprimirse en 3D. Por lo tanto las piezas deben tener un tamaño máximo de 20X20X20 cm."


**PROCESO**

Durante la especilización le vine dando mas vueltas a mi propuesta de proyecto innovador tecnológico. Por ello, empecé a direccionar mis objetivos a tener un juguete tecnológico que pueda utilizar materiales de la naturaleza como el MATE.

Parte de mi concepto, es poder identificar elemento culturales de la tradición peruana por lo que en esta primera versión me inspiré en el Señor de Sipán, mayor descubrimiento arqueológico del país, asi mismo, busco contar elementos electrónicos para vincular sonido y algo de movimiento al juguete.

Una de las oportunidades que identifiqué fue poder ubicar estos juguetes dentro del museo Tumbas Reales - Lambayeque, actual hogar del Señor de Sipán,  de tal manera que se convierta en un asistente social que acompañe a los visitantes durante su recorrido.

Es así que decidí diseñar los diferentes elementos que formarían parte de mi primer prototipo.

**REFERENCIA:**

![](../images/sp.jpg)

**PASO 1:**

Generar un modelo de los MATES que utilizaré con una medida standar (ya que al ser de origen natural es muy complicado tener formas cálculadas)
![](../images/m1.JPG)

**PASO 2:**
Diseñar la reinterpretación del tocado ceremonial del Señor de Sipán

![](../images/m2.JPG)
![](../images/m3.JPG)
![](../images/m4.JPG)
![](../images/m5.JPG)

**APRENDIZAJE:**
Al generar bocetos hechos a mano debería añadir una lista de pasos a realizar para optimizar los
proceso de modelado.

Al ser una interpretación, debo de interiorizar mejor la geometria y adaptarla al software para mejorar tiempos de trabajo.

Tengo problemas para usar la herramienta "Análisis de sección", ya que no se como dar por concluida su función.


**PASO 3:**
Diseño de elementos complementarios dentro de la propuesta: Nariguero, Aretes, detalles abstracciones iconográficas.

Todos los elementos se vinculan con el MATE por medio de 2 pines de 4mm de diametro para que se inserten dentro de la pieza central previamente agujereada.

![](../images/m7.JPG)
![](../images/m8.JPG)
![](../images/m9.JPG)
![](../images/m10.JPG)
![](../images/m11.JPG)
![](../images/m12.JPG)
![](../images/m13.JPG)

**PASO 4:**
Diseñar un case referencial donde se pueda contener la electrónica como Ardunio, sensores ( proximidad o infrarojos), luces LED, etc. El reto es poder generar una forma que pueda vincularse con la arquitectura propia de la cultura Mochica o en este caso del museo Tumbas Reales de Lambayeque.

Asi mismo, se realizaron operaciones como "hueco" y la ubicación de perforaciones para el empalme por medio de tornillos.

![](../images/m14.JPG)
![](../images/m15.JPG)
![](../images/m16.JPG)
![](../images/m17.JPG)
![](../images/m18.JPG)

**ENTREGABLE DE CONSIGNA**

**[JUGUETE TECNOLÓGICO MATE](https://drive.google.com/drive/u/0/folders/11Yph8Z6bPa8D9DGLxv-xeQronO9afXz_)**

**OTROS EXPERIMENTOS**

Quise experimientar mas funciones dentro del software como el trabajo paramétrico. Por ello, busque tutoriales para hacer modelos simples a partir de comandos, permitiendome diseñar una caja la cual puedo editar solo con la modificación de parámetros.

![](../images/p1.JPG)
![](../images/p2.JPG)
![](../images/p3.JPG)
![](../images/p4.JPG)
![](../images/p5.JPG)
![](../images/p6.JPG)
![](../images/p7.JPG)

**NOTA GENERAL / RECOMENDACIÓN:**

Quise experimentar con diseño arquitectónico en Fusion 360, pero al poner como unidades de medida: Metros, al momento de escalar cualquier parte nueva, mi procesador trabajaba al 100% de su capacidad.

![](../images/s1.JPG)
