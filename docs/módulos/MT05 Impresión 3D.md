#**MT 05** Impresión 3D y escaneo 3D


##**¿QUE ES LA IMPRESIÓN 3D?**

![](../images/I1.jpg)

La impresión 3D, también llamada manufactura por adición (inglés), es un conjunto de procesos que producen objetos a través de la adición de material en capas que corresponden a las sucesivas secciones transversales de un modelo 3D. Los plásticos y las aleaciones de metal son los materiales más usados para impresión 3D, pero se puede utilizar casi cualquier cosa, desde hormigón hasta tejido vivo.

Fuente: **[**autodesk**](https://latinoamerica.autodesk.com/solutions/3d-printing)**



##**¿QUE ES UNA IMPRESORA 3D?**

La impresión 3D es un tipo de tecnología que te permite crear un objeto tridimensional a partir de un diseño digital. Por varios siglos hemos usado las impresoras para plasmar imágenes, pero ahora también existen versiones de estas máquinas con las que podemos construir estructuras de cemento, Etc.

![](https://elmercantil.com/wp-content/uploads/2019/06/ezgif.com-crop-4.gif)

##**TIPOS DE IMPESORA 3D**

###**Modelado por deposición fundida (FDM)**

Los dispositivos de extrusión de material constituyen el tipo de impresora 3D más común y barata del mundo. Puede que conceptos como modelado por deposición fundida, (Fused Deposition Modeling, o FDM), te resulten familiares. A veces, también se hace referencia a esta técnica como fabricación con filamento fundido, (Fused Filament Fabrication, o FFF).

El mecanismo es el siguiente: se carga una bobina de filamento en la impresora 3D y se hace pasar a través de una boquilla del cabezal de extrusión. Esta boquilla se calienta hasta la temperatura deseada y, a continuación, un motor impulsa el filamento a través de ella, provocando que se derrita.

Entonces, la impresora 3D mueve el cabezal de extrusión a lo largo de las coordenadas especificadas, depositando el material fundido sobre la placa de construcción, donde se enfría y solidifica.

Una vez que se ha completado una capa, la impresora procede a colocar otra capa. Este proceso de impresión en secciones transversales se repite, construyendo capa sobre capa, hasta que el objeto está completamente formado.

Dependiendo de la geometría del objeto, a veces es necesario añadir estructuras de soporte, por ejemplo, si un modelo tiene partes que sobresalen mucho.

![](../images/I4.JPG)
[FDM](https://www.youtube.com/watch?v=IeRdk-xpRHs)

###**Polimerización VAT**

es un proceso de impresión 3D en el que una resina fotopolímera en un tanque se cura selectivamente mediante una fuente de luz. Las dos formas más comunes de polimerización VAT son la SLA (estereolitografía) y el DLP (procesamiento digital de la luz).

La diferencia fundamental entre estos tipos impresoras 3D es la fuente de luz que utilizan para curar la resina. Las máquinas SLA utilizan un láser de punto, en contraste con el enfoque por vóxels que utilizan las impresoras DLP.

Tipos de tecnologías de impresión 3D: Estereolitografía (SLA), procesamiento digital de la luz (DLP)
Materiales: Resina fotopolímera (estándar, moldeable, transparente, alta temperatura)
Precisión dimensional: ±0,5 % (límite inferior ±0,15 mm)
Aplicaciones comunes: Prototipos de polímeros similares a moldes de inyección, joyería (fundición de precisión), aplicaciones dentales, audífonos
Ventajas: Acabado liso de la superficie, precisión en los detalles
Desventajas: Frágil, no adecuado para piezas mecánicas

###**Estereolitografía (SLA)**

La estereolitografía (SLA) ostenta la distinción histórica de haber sido la primera tecnología de impresión 3D del mundo. La estereolitografía fue inventada por Chuck Hull en 1986, quien patentó la tecnología y fundó la empresa 3D Systems para comercializarla.

Las impresoras SLA utilizan espejos, conocidos como galvanómetros o «galvos», con uno posicionado en el eje X y otro en el eje Y. Estos galvos dirigen rápidamente un rayo láser sobre un tanque de resina, curando y solidificando selectivamente una sección transversal del objeto dentro de esta área de construcción, construyéndolo capa por capa.

La mayoría de las impresoras SLA utilizan un láser de estado sólido para el curado de las piezas. La desventaja de este tipo de impresora 3D, que utiliza un láser de punto, es que puede tardar más en trazar la sección transversal de un objeto que la tecnología DLP.

###**Procesamiento digital de la luz (DLP)**

Si nos fijamos en las máquinas de procesamiento digital de la luz (Digital Light Processing o DLP, por sus siglas en inglés), observaremos que este tipo de impresora 3D es casi idéntico a la estereolitografía (SLA). La diferencia clave es que el DLP utiliza un proyector de luz digital para proyectar una sola imagen sobre cada capa de una sola vez (o varias proyecciones en el caso de piezas más grandes).

Debido a que el proyector es una pantalla digital, la imagen de cada capa está compuesta por píxels cuadrados, dando como resultado una capa formada por pequeños bloques rectangulares llamados vóxels.

El DLP puede lograr tiempos de impresión más rápidos en comparación con la SLA, debido a que toda una capa se expone de una sola vez, en lugar de ir trazando el área de la sección transversal con un láser de punto.

La luz se proyecta sobre la resina mediante pantallas de diodos emisores de luz (LED) o mediante una fuente de luz ultravioleta (lámpara) que se dirigen a la superficie de construcción mediante un dispositivo digital de microespejos (DMD).

Un DMD es un conjunto de microespejos que controlan dónde se proyecta la luz y generan el patrón de luz en la superficie de construcción.

###**Estereolitografía enmascarada (MSLA)**

La estereolitografía enmascarada o MSLA utiliza una matriz de LED como fuente de luz, emitiendo luz ultravioleta a través de una pantalla de LCD que muestra una lámina de una sola capa como una máscara, de ahí el nombre.

Al igual que en el caso de la tecnología de procesado digital de luz o DLP, la fotomáscara de LCD se visualiza digitalmente y se compone de píxels cuadrados. El tamaño de los píxels de la fotomáscara de LCD define la granularidad de la impresión. Por lo tanto, la precisión XY es fija y no depende de lo bien que puedas aplicar el zoom o escalar la lente, como en el caso del procesado digital de luz. Otra diferencia entre las impresoras basadas en tecnología DLP y la tecnología MSLA es que esta última utiliza una matriz formada por cientos de emisores individuales, en lugar de un único punto de emisión de luz como un láser de diodo o una bombilla DLP.

Al igual que el DLP, la MSLA puede, bajo determinadas condiciones, lograr tiempos de impresión más rápidos que los de la estereolitografía. Esto se debe a la exposición de una capa completa de cada vez en lugar de ir trazando el área de la sección transversal con la punta del láser.

Debido al reducido coste de las unidades de LCD, la tecnología MSLA o estereolitografía enmascara se ha convertido en la tendencia en el segmento de las impresoras de resina de escritorio baratas.

###**Fusión en lecho de polvo (POLÍMEROS)**

La fusión en lecho de polvo es un proceso de impresión 3D en el que una fuente de energía térmica induce selectivamente la fusión entre partículas de polvo dentro de un área de construcción para crear un objeto sólido.

Muchos dispositivos de fusión en lecho de polvo también emplean un mecanismo para aplicar y alisar el polvo durante la fabricación de un objeto, de modo que el elemento final quede encapsulado y apoyado en el polvo no utilizado.

Tipos de tecnologías de impresión 3D: Sinterizado selectivo por láser (SLS)
Materiales: Polvo termoplástico (nailon 6, nailon 11, nailon 12)
Precisión dimensional: ±0,3 % (límite inferior ±0,3 mm)
Aplicaciones comunes: Piezas funcionales, conductos complejos (diseños huecos), producción de piezas de poca tirada
Ventajas: Piezas funcionales con buenas propiedades mecánicas, geometrías complejas
Desventajas: Tiempos de entrega más largos, mayor coste que la FFF para aplicaciones funcionales

###**Sinterizado selectivo por láser (SLS)**

La creación de un objeto mediante tecnología de fusión en lecho de polvo y polvo polimérico se conoce generalmente como sinterizado selectivo por láser (Selective Laser Sintering, o SLS). A medida que expiran las patentes industriales, este tipo de impresora 3D se está volviendo cada vez más común y menos costoso.

En primer lugar, se calienta un recipiente de polvo polimérico a una temperatura justo por debajo del punto de fusión del polímero. A continuación, una cuchilla o rasqueta de recubrimiento deposita una capa muy delgada del material en polvo (normalmente de 0,1 mm de grosor) sobre una plataforma de construcción.

A continuación, un rayo láser de CO2 comienza a escanear la superficie. El láser sinteriza selectivamente el polvo y solidifica una sección transversal del objeto. Al igual que en el caso de la estereolitografía, el láser se enfoca hacia la ubicación correcta por medio de un par de galvos.

Una vez escaneada toda la sección transversal, la plataforma de construcción se mueve hacia abajo una altura equivalente al espesor de una capa. Estos pasos se repiten hasta que todos los objetos están completamente fabricados.

El polvo que no ha sido sinterizado permanece en su lugar como soporte para el objeto que sí lo ha sido, eliminando la necesidad de estructuras de soporte.

###**Inyección de material**

La inyección de material es un proceso de impresión 3D en el que las gotas de material se depositan y curan de forma selectiva en una placa de construcción. Usando fotopolímeros o gotas de cera que se curan cuando se exponen a la luz, los objetos se construyen de capa en capa.

La naturaleza del proceso de inyección de material permite imprimir diferentes materiales en el mismo objeto. Una aplicación de esta técnica es la fabricación de estructuras de soporte a partir de un material diferente al del modelo que se está fabricando.

Tipos de tecnologías de impresión 3D: Inyección de material (MJ), Drop on Demand (DOD)
Materiales: Resina fotopolímera (estándar, moldeable, transparente, alta temperatura)
Precisión dimensional: ±0,1 mm
Aplicaciones comunes: Prototipos de productos a todo color, prototipos similares a moldes de inyección, moldes de inyección de poca tirada, modelos médicos
Ventajas: Acabado de superficie óptimo, gama completa de colores y múltiples materiales disponibles
Desventajas: Frágil, no adecuado para piezas mecánicas; mayor coste que la SLA o el DLP para fines visuales

###**Inyección de material (MJ) PLOLY JET**

La técnica de inyección de material (llamada Material Jetting, o MJ) funciona de forma similar a una impresora de inyección de tinta estándar. La diferencia clave es que, en lugar de imprimir una sola capa de tinta, se construyen múltiples capas una sobre otra para crear una pieza sólida.

El cabezal de impresión inyecta cientos de gotitas diminutas de fotopolímero y luego las cura/solidifica utilizando una luz ultravioleta (UV). Una vez depositada y curada una capa, la plataforma de construcción desciende una altura equivalente al espesor de una capa y el proceso se repite hasta construir un objeto 3D.

La inyección de material difiere de otras tecnologías de impresión 3D que depositan, sinterizan o curan el material de construcción utilizando una deposición puntual. En lugar de utilizar un solo punto para seguir una trayectoria que define el área de la sección transversal de una capa, este tipo de impresora 3D depositan el material de construcción de una manera rápida y lineal.

La ventaja de esta deposición lineal es que las impresoras de inyección de material son capaces de fabricar múltiples objetos en una sola línea sin que esto afecte a la velocidad de construcción. Siempre y cuando los modelos estén correctamente dispuestos y el espacio dentro de cada línea de construcción esté optimizado, la inyección de material puede generar piezas a un ritmo más rápido que otros tipos de impresoras 3D.

Los objetos realizados mediante inyección de material requieren de soportes, que se imprimen simultáneamente durante la construcción a partir de un material disoluble que se retira durante la fase de posprocesamiento. La inyección de material es una de las pocas tecnologías de impresión 3D que permite crear objetos de múltiples materiales y a todo color.


![](../images/I2.JPG)
[Poly Jet](https://www.youtube.com/watch?v=Som3CddHfZE&t=3s)

###**Drop on Demand (DOD)**

La técnica de «gota a demanda» o Drop on Demand (DOD) consiste en una tecnología de impresión 3D que utiliza un par de chorros de tinta: uno deposita el material de construcción, habitualmente un material parecido a la cera, y el otro, el material disoluble para los soportes. Al igual que ocurre con los típicos tipos de impresoras 3D, las máquinas DOD siguen una trayectoria predeterminada para inyectar material en una deposición puntual, creando el área transversal de un objeto capa por capa.

Las impresoras DOD también utilizan una fresa «fly-cutter» que pule el área de construcción después de crear cada capa, logrando una superficie perfectamente plana antes de comenzar la siguiente capa. Las impresoras DOD se utilizan generalmente para crear patrones adecuados para la fundición de cera perdida o la fundición de precisión, y otras aplicaciones de fabricación de moldes.

![](../images/I3.JPG)
[Drop on Demand](https://www.youtube.com/watch?v=gM86qxW7vP8)


###**Dinder Jetting (DOD)**

La inyección de aglutinante, o Binder Jetting (BJ) es un proceso de impresión 3D en el que un aglutinante líquido une selectivamente regiones de un lecho de polvo.

Se trata de una tecnología de impresión 3D similar al SLS, que requiere una capa inicial de polvo en la plataforma de construcción, pero, a diferencia del SLS, que utiliza un láser para sinterizar el polvo, la inyección de aglutinante mueve un cabezal de impresión sobre la superficie de polvo depositando pequeñas gotas de aglutinante, normalmente de 80 micrones de diámetro. Estas gotas unen las partículas de polvo para generar cada capa del objeto.

Una vez que se ha impreso una capa, se baja el lecho de polvo y se extiende una nueva capa de polvo sobre la capa recién impresa. Este proceso se repite hasta que se forma el objeto completo.

Posteriormente, el objeto se deja en el polvo para su curado y para que gane dureza. A continuación, se retira del lecho de polvo y se elimina el polvo no ligado con aire comprimido.

Tipos de tecnologías de impresión 3D: inyección de aglutinante (BJ)
Materiales: Arena o polvo metálico (acero inoxidable/bronce, arena a todo color, silicio (fundición en arena))
Precisión dimensional: ±0,2 mm (metal) o ±0,3 mm (arena)
Aplicaciones comunes: Piezas metálicas funcionales, modelos a todo color, fundición en arena
Ventajas: Bajo coste, grandes espacios de construcción, piezas metálicas funcionales
Desventajas: Las propiedades mecánicas no son tan buenas como las de piezas generadas por fusión en lecho de polvo metálico

###**Inyección de aglutinante en arena**

Los dispositivos de inyección de aglutinante en arena (en inglés Sand Binder Jetting) constituyen un tipo de impresora 3D de bajo coste que permite crear piezas a base de arena utilizando, por ejemplo, piedra arenisca o yeso.

Para crear modelos a todo color, los objetos se fabrican utilizando un polvo a base de yeso o PMMA en combinación con un aglutinante líquido. En primer lugar, un cabezal de impresión inyecta el aglutinante, mientras que un cabezal de impresión secundario inyecta el color, lo que permite imprimir modelos a todo color.

Una vez efectuado el curado de las piezas, se retiran del polvo suelto no adherido y se limpian. Para mejorar las propiedades mecánicas de las piezas, suelen exponerse a un material infiltrante.

Existe un gran número de infiltrantes disponibles, cada uno con diferentes propiedades. También se pueden añadir recubrimientos para mejorar la intensidad de los colores.

Esta técnica, basada en la inyección de aglutinante, también es útil para la creación de moldes y núcleos de fundición en arena. Generalmente, los núcleos y los moldes se imprimen con arena, aunque puede utilizarse arena artificial (sílice) para aplicaciones especiales.

Después de la impresión, los núcleos y los moldes se retiran del área de construcción y se limpian para eliminar la arena suelta. Los moldes suelen estar listos inmediatamente para la fundición. Tras esta, el molde se rompe y se retira el componente metálico final.

La gran ventaja de crear núcleos y moldes de fundición en arena mediante inyección de aglutinante es que el proceso permite crear geometrías grandes y complejas con un coste relativamente bajo. Además, el proceso resulta muy fácil de integrar en el proceso de fabricación o de fundición existente sin provocar interrupciones.

###**Inyección de aglutinante en metal**

La técnica de la inyección de aglutinante también se puede utilizar para la fabricación de objetos metálicos. El polvo metálico se une mediante un aglutinante polímero. La creación de objetos metálicos mediante inyección de aglutinante permite producir geometrías complejas mucho más allá de las capacidades de las técnicas de fabricación convencionales.

Sin embargo, solo pueden producirse objetos metálicos funcionales mediante un proceso secundario como la infiltración o el sinterizado. El coste y la calidad del resultado final generalmente definen qué proceso secundario es el más apropiado para una determinada aplicación. Sin estos pasos adicionales, una pieza metálica creada mediante la inyección de aglutinante tendría unas propiedades mecánicas deficientes.

El proceso secundario de infiltración funciona de la siguiente manera: en primer lugar, las partículas de polvo metálico se unen entre sí utilizando un aglutinante para formar un objeto en crudo («green state»). Una vez que el objeto está completamente curado, se retira el polvo suelto y se coloca en un horno, donde se quema el aglutinante. Tras este proceso, el objeto pasa a tener una densidad de alrededor del 60 % y huecos por todas partes.

A continuación, se infiltra bronce en los huecos por capilaridad, dando como resultado un objeto con una densidad de alrededor del 90 % y una mayor resistencia. Sin embargo, los objetos de metal fabricados mediante inyección de aglutinante generalmente presentan unas propiedades mecánicas inferiores a las de las piezas de metal realizadas mediante fusión en lecho de polvo.

El proceso secundario de sinterizado puede aplicarse cuando las piezas metálicas se fabrican sin infiltración. Una vez finalizada la impresión, se realiza el curado de los objetos en crudo en un horno. A continuación, se sinterizan en un horno de alta intensidad hasta alcanzar una densidad de alrededor del 97 %. Sin embargo, la contracción no uniforme puede ser un problema durante el sinterizado y debe tenerse en cuenta en la fase de diseño.

###**Fusión en lecho de polvo (metales)**

La fusión en lecho de polvo metálico es un proceso de impresión 3D que genera objetos sólidos utilizando una fuente térmica para inducir la fusión entre las partículas de polvo metálico de una capa cada vez.

La mayoría de las tecnologías de fusión en lecho de polvo emplean mecanismos para añadir polvo a medida que se construye el objeto, lo que da como resultado que el componente final quede encapsulado en el polvo metálico. Las principales variaciones en las tecnologías de fusión en lecho de polvo metálico provienen del uso de diferentes fuentes de energía, ya sean láseres o haces de electrones.

Tipos de tecnologías de impresión 3D: Sinterizado láser directo sobre metal (DMLS), fusión selectiva por láser (SLM), fusión por haz de electrones (EBM)
Materiales: Polvo de metal (aluminio, acero inoxidable, titanio)
Precisión dimensional: ±0,1 mm
Aplicaciones comunes: Piezas metálicas funcionales (para el sector aeroespacial y automovilístico), piezas médicas y dentales
Ventajas: Piezas funcionales más robustas, geometrías complejas
Desventajas: Tamaño de construcción pequeño, la tecnología con el precio más elevado

###**Sinterizado láser directo sobre metal (DMLS) / Fusión selectiva por láser (SLM)**

Tanto el sinterizado láser directo sobre metal (Direct Metal Laser Sintering, o DMLS) como la fusión selectiva por láser (Selective Laser Melting, o SLM) generan objetos de forma similar al sinterizado selectivo por láser (SLS). La principal diferencia es que estos tipos de impresoras 3D se utilizan para la producción de piezas metálicas.

El DMLS no funde el polvo, sino que lo calienta hasta un punto en el que puede fusionarse a nivel molecular. La SLM utiliza el láser para lograr una fusión completa del polvo metálico hasta formar una pieza homogénea. Este proceso da como resultado una pieza con una sola temperatura de fusión (algo que no se produce con una aleación). Este proceso da como resultado una pieza con una sola temperatura de fusión (algo que no se produce con una aleación).

A diferencia del SLS, los procesos de DMLS y SLM requieren de un soporte estructural, para limitar la posibilidad de que se produzca cualquier distorsión (a pesar de que el polvo circundante proporciona un soporte físico).

Las piezas creadas con estos tipos de impresoras 3D corren el riesgo de deformarse debido a las tensiones residuales producidas durante la impresión, a causa de las altas temperaturas. Las piezas también suelen tratarse térmicamente después de la impresión, mientras aún están adheridas a la placa de construcción, para aliviar cualquier tensión en ellas tras la impresión.

###**Fusión por haz de electrones (EBM)**

A diferencia de otras técnicas de fusión en lecho de polvo, la fusión por haz de electrones (llamada Electron Beam Melting, o EBM) utiliza un haz de alta energía, o electrones, para inducir la fusión entre las partículas de polvo metálico.

Un haz de electrones focalizado escanea una fina capa de polvo, provocando la fusión y la solidificación localizadas de un área específica de la sección transversal. Estas áreas se construyen unas sobre otras hasta crear un objeto sólido.

En comparación con los tipos de impresoras 3D SLM y DMLS, las máquinas EBM generalmente ofrecen una velocidad de construcción superior debido a su mayor densidad de energía. Sin embargo, valores como el tamaño mínimo de los detalles, el tamaño de las partículas de polvo, el grosor de la capa y el acabado de la superficie suelen ser mayores.

También es importante tener en cuenta que las piezas de EBM se fabrican en el vacío y que el proceso solo se puede aplicar a materiales conductores.

![](../images/I5.JPG)
[EBM](https://www.youtube.com/watch?v=9P2A0jF1JDg)

## Escaneo 3D

###¿Que es el escaneo 3D?

El escaneo 3D implica la recopilación de datos sobre la forma y la apariencia de un objeto físico, estructura, entorno o persona y, a continuación, el uso de esos datos para construir modelos 3D digitales. Los escáneres 3D se utilizan para analizar objetos y entornos, recopilar los datos y construir el modelo.

Existe gran variedad de diferentes técnicas de escaneo 3D pero todas se basan en los mismos principios. Todos los escáneres 3D utilizan un sensor, que puede ser una sonda física, láser o luz, para medir la distancia entre una cámara y un objeto.

Los escáneres 3D son capaces de identificar puntos 3D, calculados a partir de fotos y medición de profundidad mediante triangulación, con cada punto que aparece en la pantalla individualmente. En conjunto, estos puntos 3D forman una nube de puntos, que aparecen como en la imagen de abajo. El proceso de extrapolar la forma de un sujeto desde los puntos se conoce como reconstrucción.

### Aplicaciones del escaneo 3D

*Entretenimiento*
El escaneo 3D se utiliza para crear modelos 3D en las industrias de películas, televisión y videojuegos. En televisión y cine, a menudo se utiliza en la cinematografía virtual, donde se puede utilizar para escanear rápidamente objetos del mundo real Los artistas comúnmente esculpen modelos físicos y luego los escanean en 3D, en lugar de crear manualmente modelos digitales con software de modelado 3D.

*Medicina*
La exploración 3D es cada vez más importante en el mundo médico. Uno de los principales beneficios es que se puede utilizar en la creación y personalización de prótesis, implantes y dispositivos portátiles como moldes y aparatos ortopédicos.

*Archivo y Análisis Histórico*
El escaneo 3D es cada vez más popular entre historiadores, historiadores del arte y arqueólogos como método de documentación y análisis. El escaneo 3D crea modelos 3D reproducibles y altamente precisos de artefactos y obras de arte, por lo que es una técnica importante en archiving y curación. Además, permite el intercambio de copias precisas en todas las comunidades de investigación y enseñanza.

*Diseño*
La creación rápida de prototipos y la ingeniería inversa son muy importantes para el proceso de diseño de muchos productos, por lo que el escaneo 3D es una herramienta útil para muchos diseñadores. Cuando los diseñadores buscan imitar materiales naturales, el escaneo 3D proporciona una manera rápida y precisa de estudiar arreglos complicados y crear nuevos diseños.

### Técnicas del escaneo 3D

####Escaneo 3D de triangulación láser
Este tipo de escaneo 3D utiliza un único punto láser o una línea láser para escanear un objeto. El láser es lanzado por el escáner 3D y refleja el sujeto, modificando su trayectoria inicial. El cambio de trayectoria es registrado por un sensor y permite al sistema calcular el ángulo específico de desviación utilizando triangulación trigonométrica. Con suficientes escaneos láser, el escáner puede mapear la superficie del sujeto y crear un escaneo 3D.

####Escaneo 3D de luz estructurada
Al igual que los escáneres de triangulación láser, los escáneres de luz estructurados también utilizan triangulación trigonométrica para crear escaneos. Sin embargo, en lugar de usar un láser, esta técnica proyecta una serie de patrones lineales de luz en un objeto. Dos cámaras de detección analizan los patrones de luz, examinando la longitud de cada línea para calcular la distancia al sujeto. A continuación, el escáner puede calcular las coordenadas X-Y-Z exactas para crear modelos 3D extremadamente precisos.

####Escaneo 3D basado en pulso láser
También conocido como escaneo de tiempo de vuelo, este método crea escaneos utilizando la velocidad de la luz y los sensores en lugar de la triangulación. Los escáneres 3D basados en pulso láser hacen esto midiendo cuánto tiempo tarda un láser fundido en alcanzar un objeto y volver a través de la reflexión. El hardware del láser y del sensor es girado por un espejo, lo que permite al software recopilar datos de 360 grados y capturar la información necesaria para crear un modelo 3D.

####Fotogrametría
La fotogrametría es una técnica para obtener mediciones y otra información dimensional de objetos y entornos a partir de fotografías de alta resolución. La fotogrametría se puede hacer utilizando cámaras aéreas o portátiles que se utilizan para tomar una serie de fotos superpuestas del sujeto. El software de fotogrametría es entonces capaz de crear modelos 3D por punto que coincidan con la intersección geométrica de los rayos de luz y utilizar la triangulación para descifrar información como ángulos de cámara, ubicaciones y características del sujeto.



##CONSIGNA
"Modela (opcional escanear) un elemento en 3d que sea de utilidad para tu proyecto final o que pueda ser utilizado como insumo para su desarrollo. Documenta los distintos pasos del proceso en tu página web integrando los errores y aprendizajes obtenidos.

NOTA: Este módulo implica la producción de una pieza en un equipo de FDM por lo que el modelo debe entregarse en código G y en STL siguiendo las referencias detalladas a continuación.

Software para SLICING - CURA

Equipo para el que se realiza el Slicing - Ultimaker 3

Medidas 60x60x60mm máximo (es una prueba de impresión)

Variables a definir por parte del estudiante:

- Altura de capa
- Densidad de relleno
- Soporte
- Material
- Ventilación
- Velocidad
- Posición del objeto

**Las variables que debe definir el estudiante están listadas como ejemplo, pueden haber más variables a definir. "



###PARTE A IMPRIMIR
![](../images/I6.jfif)

Se seleccionó el tocado de la primera versión de juguete tecnológico, ajustandose algunas medidas de acuerdo a los lineamientos aprendidos en clase.

![](../images/I7.jfif)

 Se engrosó el espesor de la parte superior del tocado, asi como el diametro y largo del pin de unión entre este con el mate.

![](../images/I8.jfif)


![](../images/I9.JPG)

Se procedió a exportar el archivo en formato Stl, refinandolo en la mayor capacidad posible para una mejor resolución.

###STL

[ARCHIVO STL - TOCADO ](https://drive.google.com/drive/u/0/folders/1xLmQ5KKBJj41lW9SmTDzxYmzR89ebH9k)

![](../images/I10.JPG)

Para corroborar el punto anterior, se procedió a revisarlo en el visualizador de windows, donde efectivamente tiene una buena resolución, pero se encuentra en una horientación que dificulta observarlo bien.

*Nota:* *no encuentro la opción para girar la horientación del objeto en el visor 3D de windows*

###SLICING 3D - CURA

![](../images/I11.JPG)



Se procedió a instalar CURA, configurar la impresora: ULTIMAKER 3 e importar el archivo STL del tocado.

![](../images/I12.JPG)

 Se escaló la pieza a 60 mm de alto, aunque en proporción para no deformar la pieza.

![](../images/i13.JPG)

Se procedió a modificar los parametros, se busca tener una buena resolución y buena resistencia por lo que se puso una altura de capa de 1.8 mm , 3 pasadas de grosor y infill de 20% con patron triangular.

 ![](../images/I14.JPG)

 Al tener un diseño casi hueco en la base, se optó por utilizar relleno en el slicer. Posteriormente se procedió a laminar.

![](../images/I15.JPG)
![](../images/I16.JPG)
![](../images/I17.JPG)

Se puede observar la ubicación del soporte con relación a la pieza a imprimir, teniendo una buena presencia en la misma al tener una geometria dinstante entre la base y el extremo mas alto.

![](../images/I18.JPG)
![](../images/I19.JPG)

###ARCHIVO EN CODIGO G

[ARCHIVO G- Code ](https://drive.google.com/drive/u/0/folders/1K1qqfzhyMTpLrGzrh3P1cmgd7j26DBvp)

###PRUEBAS DE CONFIGURACIÓN DE FLASHFORGE DREAMER EN CURA

![](../images/I20.jfif)

Actualmente cuento con una impresora flashforge dreamer, sin embargo CURA no cuenta con este modelo en su repositorio de configuraciones de impresoras. Por ello, se procedió a ser una configuración personalizada.

![](../images/I22.jfif)

Mi impresora no reconoció el archivo por lo que investigué y resulta que al ser código cerrado, tengo que esperar la actualización o en su defecto no realicé bien la configuración.

![](../images/I21.jfif)

Paso siguiente probé con el SLICER por default de la misma impresora: FLASHPRINT

![](../images/I25.JPG)

Puse parametros para tener un acabado de calidad media y hacer la prueba

![](../images/I26.JPG)
![](../images/I27.JPG)

Obteniendo este resultado, el cabado no es como yo esperaba, rompiendose el pin de conexión y teniendo terminaciones un poco toscas, ya que no le puse soportes .

![](../images/I29.jfif)
![](../images/I30.jfif)
![](../images/I31.jfif)
![](../images/I32.jfif)

##SEGUIMOS EXPERIMENTANDO

Me aventuré en hacer nuevas pruebas, modificando variables de calor en cama y velocidad de retracción. Sin embargo, me he quedado sin filamento y actualmente mi proveedor no me ha estado contestando.

![](../images/0001.JPG)
![](../images/0002.JPG)

Para mejorar la calidad, le reduje la altura de capa para aumentar la resolución. Como resultado, ha aumentado mis tiempos de impresión.
![](../images/0003.JPG)


###Recomendaciones de clase

Tuve algunos problemas con la retracción del por lo que el resultado final de la impresión 3D no era tan prolijo como estaba buscando. Por ello se me recomendó revisar esta web para buscar la solución a mi problema.

[Problemas frecuentes 1](https://www.simplify3d.com/support/print-quality-troubleshooting/)

[Problemas frecuentes 2](https://support.3dverkstan.se/article/23-a-visual-ultimaker-troubleshooting-guide)

Asi mismo, para obtener un acabado mas liso en las piezas impresas, se me recomendó realizar un proceso de post- impresión. Se añade el link de referencia.

[Problemas frecuentes 3](https://www.smooth-on.com/product-line/xtc-3d/)

Sin embargo, ese producto no se encuentra disponible en mi pais, por lo que busqué alternativas.

[![Acabdo con resina EPOXI](../images/epox1.JPG)](https://www.youtube.com/watch?v=40SqibP7Tbc)
