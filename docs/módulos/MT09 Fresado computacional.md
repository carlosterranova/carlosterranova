#**MT 09** Fresado computacional

![](../images/c1.jpg)

### CNC
El proceso consiste en diseñar un programa con un conjunto de órdenes añadidas que determinen la posición y rotación de la herramienta de forma secuencial. De este modo, podemos controlar su posición y velocidad de desplazamiento exacta respecto al material que se usa para la fabricación de las piezas mecanizadas.

![](../images/C2.JPG)
[COMO FUNCIONA UNA CNC](https://www.youtube.com/watch?v=jOJkZgz5i9Y)

###El proceso de fabricación utilizando tecnología CNC

La primera fase del trabajo consiste en el diseño de la pieza que se quiere fabricar mediante maquinaria CNC. Habitualmente se realiza mediante un programa de dibujo asistido por ordenador CAD.

Una vez diseñada la pieza, se introducirán en la máquina herramienta las instrucciones necesarias para su fabricación. Estas instrucciones son las que forman el programa CNC, escrito en un lenguaje específico y estandarizado. Mediante el uso del código, se deben definir secuencialmente todos los pasos a seguir por la herramienta: posicionamiento mediante coordenadas, dirección y velocidad de avance, profundidad, arranque o pausa de la herramienta, etc.

El programa de apoyo CAM (fabricación asistida por computadora) en combinación con el programa de dibujo asistido CAD permiten crear automáticamente el programa CNC que será introducido en el módulo de control de la máquina-herramienta.

###Secuencia del proceso CNC

La primera fase del trabajo consiste en el diseño de la pieza que se quiere fabricar. Habitualmente se realiza mediante un programa de dibujo asistido por ordenador CAD.



Una vez diseñada la pieza, se introducirán en la máquina herramienta las instrucciones necesarias para su fabricación. Estas instrucciones son las que forman el programa CNC. Está escrito en un leguaje específico y estandarizado. Su formato es en forma de código con claves determinadas, que permiten definir cada movimiento o acción de forma secuencial:

 -Posicionamiento, introduciendo las coordenadas correspondientes con los puntos de trabajo de la herramienta sobre la pieza a mecanizar y su posición con respecto a la máquina-herramienta. Cada punto estará referido a su posición con respecto los ejes X, Y, Z.
-Velocidad de avance.
-Profundidad.
-Arranque o pausa de la herramienta.
-Cambio de herramienta.
-Variables direccionales.
-Arranque o paro.

###TIPOS DE CNC POR EJES

![](../images/C3.JPG)
![](../images/C4.JPG)
![](../images/C5.JPG)
![](../images/C6.JPG)
![](../images/C7.JPG)

###PARTES
![](../images/C8.JPG)
![](../images/C9.JPG)

###TOOLPATH

Un toolpath es la ruta codificada definida por el usuario que sigue una herramienta de corte para mecanizar una pieza.
Se representan en la pantalla mediante líneas y curvas que representan la trayectoria del centro inferior de la herramienta de corte. Los toolpath de pocketing graban la superficie del material,
mientras que los toolpath de perfil cortan todo el material.

![](../images/C10.JPG)

###G CODE

El programa CNC está escrito en un lenguaje de bajo nivel denominado G y M, estandarizado por las normas 6983 de ISO

(Organización Internacional de Normalización) y RS274 de EIA (Alianza de Industrias Electrónicas)

Compuesto por instrucciones Generales (código G) y Misceláneas (código M). El programa presenta un formato de frases conformadas por bloques, encabezados por la letra N, tal como vemos en la figura de abajo, donde cada movimiento o acción se realiza secuencialmente y donde cada bloque está numerado y generalmente contiene un solo comando.

 ![](../images/C11.JPG)

###TERMINOS

**ROUGHENING** DESBASTAR GRAN PARTE DEL MATERIAL PERO NO TODO
**FINISHING** MECANIZAR EL RESTO HASTA EL MODELO DESEADO
**PROFILE** CORTAR POR UNA LÍNEA
**STEP OVER** CUANTA HERRAMIENTA ESTÁ CORTANDO
**STEP DOWN** PROFUNDIDAD DE CORTE EN UNA PASADA
**FEED** VELOCIDAD DE CORTE
**PLUNGE** CORTE VERTICAL
**LEAD IN/ENGAGE** COMO HACER LA ENTRADA AL MATERIAL
**POCKETING** CORTE DE VACIADO DENTRO DE UNA FORMA CERRADA
**FACING** APLANAR TODA LA CARA
**STOCK** PIEZA QUE SE DESEA CORTAR

###BROCAS

![](../images/C12.JPG)
![](../images/C13.JPG)

###HOLD DOWN
Particularmente para láminas delgadas de material, es importante
asegurar muy bien el trabajo. La vibración excesiva del material
generará calor adicional y provocará una falla prematura de la broca o un
acabado superficial deficiente.
Sin embargo, también es importante no apretar demasiado
los tornillos, ya que provocarán que el material se combe, lo que puede
afectar la precisión dimensional. Los tornillos de cabeza plana son
mejores que los tornillos de cabeza cónica aquí.

![](../images/C14.jfif)
![](../images/C15.JPG)
![](../images/C16.JPG)
![](../images/C17.JPG)
![](../images/C18.JPG)

###SOFTWARE
####EASEL
Easel es un software de Inventables de CAD y CAM en el
navegador. Permite diseñar y generar archivos para CNC de
una manera fácil e intuitiva.
Está diseñado para usuarios principiantes que permite
controlar los principales ajustes para la fresadora CNC.

![](../images/C19.JPG)
[TUTORIAL EASEL](https://www.youtube.com/watch?v=BGSNymNK9ZY)

####OTROS SOFTWARE

**ASPIRE** www.vectric.com

**Free Mill** www.mecsoft.com

**HSMexpress** www.hsmworks.com

**G-simple** www.gsimple.eu

**Heeks CAD-CAM** www.heeks.net

**EMC2** www.linuxcnc.org

**Jedicut** www.jelinux.pico-systems.com

**FABMODULES** www.fabmodules.org

**Ace Converter** www.dakeng.com/ace.html

**DeskEngrave** www.deskam.com

**Modela4player** www.rolanddg.com

**Auto-Trace** www.autotrace.sourceforge.net

**Scan2CNC** www.gravomaster.com

**MaxCut** www.maxcut.com.za

**Rhino CAM** www.rhino.com


### CONSIGNA

Modelar algo mas grande de 1mx1mx1m y que utilice uniones de madera.
Sin tornillos, sin cola, solo uniones secas
Haz los toolpaths en Easel,Fusion 360 o el softwares que elijas.

### PROCESO

####MODELADO EN FUSION 360
![](../images/cnc0.png)

![](../images/cnc1.png)

[ARCHIVO MODELADO 3D](https://drive.google.com/file/d/16TWuFPdwMDemV1SiMz5fj3HWN4tRIo76/view?usp=sharing)

####DFX EN FUSION 360
![](../images/cnc1.1.png)

[ARCHIVO DXF](https://drive.google.com/file/d/1qVbAmfl1Hb3qw06Y7HG8svUs_gw06aZR/view?usp=sharing)

####EASEL

![](../images/cnc2.png)
![](../images/cnc3.png)
![](../images/cnc4.png)
Modifique los parametros de profundidad, unidad de medida (mm / Ing), tipo de corte (por fuera de la línea) y dimensiones generales.

#### CÓDIGO G

 [CÓDIGO G roughing ](https://drive.google.com/file/d/1yreWcIob92veiRS0UKf8kc9FYQgRjwRj/view?usp=sharing)

  [CÓDIGO G Details ](https://drive.google.com/file/d/1UohZvNRtlxUjmovhoSsKo76uBeTj5l6l/view?usp=sharing)
