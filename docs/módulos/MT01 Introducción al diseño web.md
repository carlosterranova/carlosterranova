#**MT 01** Introducción al diseño web - Herramientas digitales

![](../images/0.3.JPG) 

##**Breve historia de Git**
Como ocurre con muchas cosas maravillosas en la vida, Git comenzó con un poco de destrucción creativa y una feroz controversia.

 El Linux kernel es un proyecto de software de código abierto de alcance bastante amplio. En 2002, el proyecto del Linux kernel comenzó a utilizar un DVCS propietario llamado BitKeeper.

En 2005, la relación entre la comunidad que desarrolló el Linux kernel y la empresa comercial que desarrolló BitKeeper se rompió y finalizó el estado gratuito de la herramienta. Esto llevó a la comunidad de desarrollo de Linux (y en particular a Linus Torvalds, el creador de Linux) a desarrollar su propia herramienta basada en algunas de las lecciones que aprendieron mientras usaban BitKeeper. Algunos de los objetivos del nuevo sistema fueron los siguientes:

Velocidad
Diseño simple
Fuerte apoyo para el desarrollo no lineal (miles de ramas paralelas)
Totalmente distribuido
Capaz de manejar proyectos grandes como el Linux kernel de manera eficiente (velocidad y tamaño de datos)
Desde su nacimiento en 2005, Git ha evolucionado y madurado para ser fácil de usar y, sin embargo, conservar estas cualidades iniciales. Es increíblemente rápido, es muy eficiente con grandes proyectos y tiene un increíble sistema de ramificación para el desarrollo no lineal.


Como parte del proceso, Se utilizó herramientas como ATOM y GIT Bash para la programación de esta página web / repositorio.

##**Git**
La principal diferencia entre Git y cualquier otro VCS es la forma en que Git maneja los datos. Conceptualmente, la mayoría de los otros sistemas almacenan información como una lista de cambios basados ​​en archivos. Otros sistemas (CVS, Subversion, Perforce, Bazaar, etc.) manejan la información que almacenan como un conjunto de archivos y los cambios realizados en cada archivo a lo largo del tiempo (esto se describe comúnmente como control de versiones basado en delta).

![](../images/mt01.1.png)

##**¿GitLab?**

Gitlab es un servicio web de control de versiones y desarrollo de software colaborativo basado en Git. Además de gestor de repositorios, el servicio ofrece también alojamiento de wikis y un sistema de seguimiento de errores, todo ello publicado bajo una Licencia de código abierto.

GitLab es una suite completa que permite gestionar, administrar, crear y conectar los repositorios con diferentes aplicaciones y hacer todo tipo de integraciones con ellas, ofreciendo un ambiente y una plataforma en cual se puede realizar las varias etapas de su SDLC/ADLC y DevOps.

Fue escrito por los programadores ucranianos Dmitriy Zaporozhets y Valery Sizov en el lenguaje de programación Ruby con algunas partes reescritas posteriormente en Go, inicialmente como una solución de gestión de código fuente para colaborar con su equipo en el desarrollo de software. Luego evolucionó a una solución integrada que cubre el ciclo de vida del desarrollo de software, y luego a todo el ciclo de vida de DevOps. La arquitectura tecnológica actual incluye Go, Ruby on Rails y Vue.js.

[CREAR UNA CUENTA EN GITLAB](https://docs.google.com/presentation/d/17I7tGMCiu8yuugg000N9HiJ5-BgRNPAPlTqJCPAOqaI/edit#slide=id.g629a3b6be0_0_25)

[INSTALAR GIT](https://docs.google.com/presentation/d/1X1QMYCr4WBCqELYorc-7jfgMxZKQV0kof8FMsHTLfJ4/edit#slide=id.g629a3b6be0_0_8)

[GIT GLOBAL SETUP](https://docs.google.com/presentation/d/1SeEpfWPef96sFX_8AKj7VtSAdVWbHybC417_ekahHi4/edit#slide=id.g629a3b6be0_0_157)

[GENERAR UN SSH KEY](https://docs.google.com/presentation/d/1_mGalcjQXmzZHSA3_DizcWGtCSjgqf0jr-XMOa-LA3I/edit#slide=id.g629a3b6be0_0_46)

[FORK TEMPLATE](https://docs.google.com/presentation/d/1NlBlnsoGawYtwx-jDZZVuKNU3SMGTWm3gBwSQjEg8Bw/edit#slide=id.g629a3b6be0_0_163)

**¿Que es ATOM?**

Atom es un editor de código fuente de código abierto para macOS, Linux, y Windows1​ con soporte para múltiples plug-in escritos en Node.js y control de versiones Git integrado, desarrollado por GitHub. Atom es una aplicación de escritorio construida utilizando tecnologías web.2​

La mayor parte de los paquetes tienen licencias de software libre y está desarrollados y mantenidos por la comunidad de usuarios.3​ Atom está basado en Electron (Anteriormente conocido como Atom Shell), Un framework que permite crear aplicaciones de escritorio multiplataforma usando Chromium y Node.js.4​5​6​ Está escrito en CoffeeScript y Less.7​ También puede ser utilizado como un entorno de desarrollo integrado (IDE).8​9​10​ Atom liberó su beta en la versión 1.0, el 25 de junio de 2015.11​

Sus desarrolladores lo llaman un «editor de textos hackeable para el siglo XXI».12​

Cabe resaltar que Atom puede añadir soporte para otros lenguajes de programación mediante el sistema de paquetes, así también mejorar el soporte para los lenguajes existentes mediante mejoras como intérpretes, debbugers o pipelines que conecten software de terceros a Atom.

[DESGARGA DE ATOM](https://atom.io/)


##**¿Que es MARKDOWN?**

Markdown es un lenguaje de marcado que facilita la aplicación de formato a un texto empleando una serie de caracteres de una forma especial. En principio, fue pensado para elaborar textos cuyo destino iba a ser la web con más rapidez y sencillez que si estuviésemos empleando directamente HTML. Markdown es realmente dos cosas: por un lado, el lenguaje; por otro, una herramienta de software que convierte el lenguaje en HTML válido.





## **CONSIGNA**

"Crear tu sitio web personal y documentar que herramientas utilizaste.
Escribir un about. Documentar tu proceso en este módulo y en las consignas de los módulos anteriores (MP01-MI-EMP). Trabajar con un tutorial de git."

##**Primeras aproximaciones con GIT y ATOM**

De acuerdo a la consigna, se realizó las tareas como la instalación del software GIT BASH, para sicronizar mi ordenador con GITLAB. Es asi como ser puede nutrir el repositorio desde la PC.
![](../images/mt01.3.JPG)

Posteriomente, se intaló el software ATOM como interfase principal de trabajo y que permite la actualización constante del repositorio.

![](../images/programacion.JPG)

Cuando realizo un cambio dentro de ATOM, puedo visualizar el proceso de subida  entrando a la ruta https://gitlab.com/carlosterranova/carlosterranova/-/pipelines, para posteriormente verlo ya implementado en este repositorio web.  

![](../images/git1.JPG)

##**Algunos recursos**
Al inicio tuve algunos problemas para diferenciar textos, por lo que busqué algunos recursos en google. Encontré este link que me muestra los comandos, siendo básicamente los mismos que GITHUB.

![](../images/git3.JPG)



[Escritura GITHUB](https://docs.github.com/es/github/writing-on-github/getting-started-with-writing-and-formatting-on-github/basic-writing-and-formatting-syntax)


**NOTA**

Problema 1: Presenté problemas para ingresar correctamente al repositorio web, figurando como una página malisiosa por tener en la extensión "Carlos.terranova" un punto; siendo este carater el causante del fallo.

Problema 2: Intento ponerle el nombre de los módulos de aprendizaje al lado del cógido, sin tener éxito en todos los casos.

![](../images/mt01.4.JPG)

**NOTA 2**

Luego de los inconvenientes de hechos pasados, el error se encontraba en haber agregado nuevos caracteres en la pestaña MKDOCS.YML, especificamente al cambiar la extensión del copyright.

![](../images/mkdoc.JPG)

**NOTA 3**

Como parte del proceso de restauración del repositorio web, se descargó el software FORK para visualizar si se ha habia vinculado correctamente el ordenador con GITLAB.

![](../images/fork.JPG)

##***Seguimos trabajando***
