
#**MI** Emprendimiento

![](../images/0.8.JPG)

##Sesión uno - Dia uno

###¿Que es el bussines model canvas BMC?

En términos generales, el término técnico " Business Model Canvas " significa que usted puede presentar claramente su idea de negocio en un pedazo de papel. Esto le permite visualizar su modelo de negocio, detectar debilidades y probar si puede funcionar. El BMC fue desarrollado por el teórico económico suizo Alexander Osterwalder y el informático Yves Pigneur en 2005.

![](../images/bmc4.JPG)

![](../images/bmc2.JPG)

###¿Cuáles son las ventajas de un BMC?
Ya sea que esté trabajando en su modelo de negocio existente o en una nueva idea, el BMC tiene cinco ventajas principales:


### Beneficios del BMC

 **1. Una mirada a lo esencial**

Con el BMC siempre tendrá una visión general de lo que realmente es su modelo de negocio. También muestra qué actividades no son tan importantes o incluso se interponen en el camino de la meta. Debido a que el BMC afina su visión de lo esencial, usted puede mejorar aún más su modelo de negocio.

**2. Base para la lluvia de ideas**

El BMC es perfecto como base para dejar que tus pensamientos corran libremente. También puede probar modelos alternativos para su idea de negocio y encontrar nuevas posibilidades de combinación.

**3. Presentación estructurada**

El BMC lleva su idea de negocio a una forma clara y estructurada. Esto tiene la ventaja de que otros pueden entender y discutir rápidamente su modelo de negocio. El BMC ofrece una visión general de su idea de negocio desde cualquier perspectiva. Así, por ejemplo, el departamento de control, el departamento de estrategia o la dirección reciben rápidamente la información exacta que es importante para ellos.

**4. Dependencias y Conflictos de Objetivos**

Con el BMC, las startups pueden presentar su idea de negocio. Las empresas también pueden utilizar el BMC para nuevos proyectos. El BMC también puede mostrar dependencias o conflictos de objetivos con los procesos existentes.

**5. Base**

El BMC no puede reemplazar un plan de negocios detallado para su idea. Pero si usted presenta su modelo de negocio en un BMC, también obtiene una estructura para su plan de negocios.

Hemos desarrollado una plantilla alemana del Business Model Canvas con preguntas útiles, que puede descargar de forma gratuita contactando con LEAD Innovation management.

Luego de una semana de workshop, se pidió como entregable el esquema del business model canvas y un diagrama de Gantt para marcar la dirección del proyecto final.

**[BMC MIRO](https://miro.com/app/board/uXjVObUJ0ro=/)**

## DIAGRAMA DE GANTT

El diagrama de Gantt es una herramienta de gestión que sirve para planificar y programar tareas a lo largo de un período determinado. Gracias a una fácil y cómoda visualización de las acciones previstas, permite realizar el seguimiento y control del progreso de cada una de las etapas de un proyecto y, además, reproduce gráficamente las tareas, su duración y secuencia, además del calendario general del proyecto

El diagrama de Gantt resulta muy útil para comunicarse con el resto del equipo y, por eso, es frecuente que se ponga en común y se vaya actualizando, a medida que se producen cambios.

### USO DE DIAGRAMA DE GANTT

**1. Diagrama de Gantt en proyectos de construcción**. Es habitual pensar en esta herramienta para la gestión de proyectos y, en este caso, puede suponer un gran apoyo para la gestión de contratas y el seguimiento de los trabajos. Se trata de un diagrama muy fácil de usar que puede agrupar fácilmente todas las etapas de construcción en las columnas, permitiendo editar las tareas y personalizarlas según las necesidades.

**2. Diagramas de Gantt en TI**. En un entorno ágil por definición, quienes saben qué es un diagrama de Gantt lo emplean para comparar las actividades programadas y los recursos requeridos. La visualización de proyectos así planteada crea un reflejo transparente incluso de las estructuras más complejas con sus dependencias.

**3. Diagrama de Gantt en educación**. Docentes, programas e hitos académicos pueden reflejarse en este gráfico, que, además, facilita la compartición de información de manera transparente, ya que puede reflejar estadísticas descriptivas simples y datos demográficos departamentales.

**4. Diagramas de Gantt en marketing**. En esta área se utiliza para gestionar eventos, ya que el gráfico permite aplicar diferentes escalas de tiempo para las distintas fases de un evento de marketing. Su uso hace posible aumentar la eficiencia en la gestión y permanecer actualizado. Además, resulta la herramienta perfecta para facilitar la adaptación en un entorno tan cambiante como éste, ya que facilita incorporar cambios de última hora sin que ello suponga un problema de cara al cronograma.

**[GANTT EN GOOGLE SLIDE](https://docs.google.com/spreadsheets/d/1AQ29tg_Rf70jkreNERpkFYWSLuZHVaZXYMhSw0qgopQ/edit#gid=0)**

##Sesión dos - Dia dos

### Design Thinking

El Design Thinking se presenta como una metodología para desarrollar la innovación centrada en las personas, ofreciendo una lente a través de la cual se pueden observar los retos, detectar necesidades y, finalmente, solucionarlas. Se trata de dar un enfoque que se sirve de la sensibilidad del diseñador y su método de resolución de problemas. El objetivo: satisfacer las necesidades de las personas de una forma que sea tecnológicamente factible y comercialmente viable.

### Fases del Design Thinking

**Fase 1: tener empatía**

Investigar las necesidades de tus usuarios. Se trata de entender empáticamente el problema que está tratando de resolver. Por lo tanto, esta fase suele comenzar por la investigación del usuario o consumidor.

**Fase 2: definir**

Identificar las necesidades y los problemas de sus usuarios. Es hora de poner sobre la mesa la información recopilada durante la primera etapa.  Se analizan todas las observaciones y se sintetizan para definir los problemas centrales que el equipo ha identificado.

**Fase 3: idear**

En esta fase, se trata de generar cuantas ideas sea posible. Se trata de “pensar fuera de la caja”, buscar formas alternativas de ver el problema e identificar soluciones innovadoras para el planteamiento del problema.

**Fase 4: prototipar**

Construir soluciones. Inicialmente se identifica la mejor solución para cada problema encontrado. A continuación, el equipo tiene que construir versiones económicas y muy primitivas de lo que sería el producto final, aunque sea en papel.

**Fase 5: probar**

Probar los prototipos es la última de las fases del proceso de Design Thinking. Existe una figura –la de los evaluadores–, que se encarga de probar rigurosamente los prototipos. Aunque esta es la fase final, la metodología del design Design Thinking se basa en un modelo de trabajo iterativo. A raíz de los resultados obtenidos, los equipos a menudo utilizan ese como punto de partida. Por lo tanto, es posibles volver a las etapas anteriores para realizar más iteraciones, alteraciones y refinamientos.

### Características

***La generación de empatía***. Hay que entender los problemas, necesidades y deseos de los usuarios implicados en la solución que estamos buscando. Independientemente de qué estemos desarrollando, siempre conllevará la interacción con personas. Satisfacerlas es la clave de un resultado exitoso.

***El trabajo en equipo***, ya que pone en valor la capacidad de los individuos de aportar singularidad.

***La generación de prototipos***, ya que defiende que toda idea debe ser validada antes de asumirse como correcta. La metodología del Design Thinking propicia la identificación de fallos, para que cuando demos con la solución deseada, éstos ya se hayan solventado.

![](../design1.jpg)

##Sesión tres - Dia tres

### Design sprint de Google

Un Design Sprint es un proceso paso a paso que en solo 5 días permite crear y validar nuevas ideas y resolver grandes retos de negocio. La metodología fue creada en Google por ***Jake Knapp*** quien luego la compartió con el mundo a través de su libro **Sprint**, que fue publicado en 2016 y se convirtió en un bestseller internacional.

Los **Design Sprints 2.0** son una versión optimizada -validada por Jake Knapp- que acorta el proceso a solo 4 días.

###¿Como es un Sprint?

El lunes se identifica el reto a resolver y se proponen distintas soluciones. El martes se decide en qué camino seguir y se convierten las ideas en una hipótesis comprobable. El miércoles se elabora un prototipo realista que se usará para validar con usuarios reales el jueves.

![](../sprint.jpeg)

###¿Cuando debemos de usar un Sprint?

**Lanzar un nuevo producto o servicio** ya que alinean al equipo en una dirección clara y hacen más corto y eficiente el ciclo de desarrollo de un producto o la resolución de un problema. Además permiten validar la idea antes de invertir tiempo y dinero en su desarrollo y lanzamiento.

**Hacer un pitch “tangible”** un Design Sprint permite presentar una idea de manera tangible y validada con usuarios reales; de esta forma es mucho más fácil convencer a stakeholders o potenciales inversores.

**Ponerse manos a la obra** cuando se necesita solucionar un tema importante, pero el día a día nunca deja tiempo para ello, hacer un Design Sprint es una gran manera de ponerse manos a la obra. Permite parar el resto de cosas y focalizarse realmente en conseguir los resultados deseados.

**Alineación & teambuilding** es muy útil también cuando el equipo, los stakeholders o la compañía no son capaces de alinearse en un tema determinado y está tomando demasiado tiempo ponerlos a todos de acuerdo; o cuando se reorganizan equipos y se necesita cohesionarlos y romper silos.

**Gran apuesta** cuando la compañía está pensando en hacer una gran apuesta desarrollando un nuevo producto o servicio que le costará muchísimo dinero, un Design Sprint permitiría tener una idea clara y validada acerca de la dirección que hay que tomar antes de realizar una gran inversión.

##Sesión cuatro - Dia cuatro

### Performance marketing

El Performance Marketing es “un modelo de marketing online en el que el anunciante sólo paga por los resultados conseguidos”.

El concepto lo desarrolló la misma Performance Marketing Association, a donde pertenecen grandes empresas como Google, Yahoo!, Dell, Ebay y Amazon.

Así que este tipo de modelo de Marketing puede funcionar únicamente para compañías que tengan un volumen considerable, porque sino el gasto humano y el tiempo invertido para llevar a cabo una campaña sería mayor que el beneficio que se espera.

Por tanto, este proceso se usa básicamente en Internet para poder definir qué necesita un anunciante para poder conseguir sus resultados.

Resulta necesario, entonces, contar con herramientas precisas de tracking de resultados que tengan la posibilidad de almacenar los clics, visitas, suscripciones o ventas en tiempo real.

De esta manera y ya que la compañía se encuentra en un entorno digital, obtendrá los datos precisos sobre si se están obteniendo resultados positivos o no.

**NOTA**:
**Tranking de resultados**  significa seguimiento o rastreo en inglés, es una de las cuestiones más importantes y una de las más descuidadas de las campañas de marketing offline. Se trata de detectar qué anuncio ha sido el que ha generado una llamada o una venta.


#### TIPS

El que solo se haga el pago por los resultados obtenidos debe dar una alerta de que no importa los canales mediante los cuales se esté trabajando, lo importante son los resultados.

Y es que por una razón al Performance Marketing también se le denomina como el Marketing de resultados.

Por lo tanto, quien se dedique a hacer Performance Marketing debe ser cuidadoso de ciertos aspectos.

***Objetivos:*** deben ser planteados de la manera más clara y medible posible.
No tiene lógica aplicar un Performance Marketing que se caracteriza por dar resultados a objetivos como “ser la mejor marca del mercado”, por poner un ejemplo.

Por otro lado, los KPI’s a cumplirse deben estar claramente aclarados para la agencia que llevará a cabo la campaña.

***Modelo de pago:*** hay diversos tipos de modelos de pago, pero este aspecto se debe definir muy bien puesto que aunque se usen diversos canales, todo va a depender de la conversión.
En una campaña de este modelo de Marketing, suelen emplearse los siguientes modelos de pago:

***CPL*** (Coste por Lead).

***CPA*** (Coste por Acción o Coste por Adquisición).

***CPC*** (Coste por clic) En el modelo de CPC, el anunciante no paga en función de la audiencia que ve un anuncio, sino en función del usuario que responde al anuncio, realizando un clic y manifestando así su interés en visitar la web del anunciante para saber más.

***Landing pages:*** Es importante que se mantengan las landing pages optimizadas, ya que de nada sirve invertir en una estrategia en donde email marketing o redes sociales redireccionen a una página que ni siquiera está actualizada.


***Especialistas en Performance Marketing:*** hay especialistas en estas tecnologías como conversion designer, experto en Adwords y social Ads o  landing conversión manager.
Ellos pueden apoyar de sobremanera en una campaña de este modelo de Marketing puesto que sabrán cómo usar las herramientas digitales que puedan ayudar al logro de los resultados esperados.

***Monitorización:*** la monitorización es fundamental en el Performance Marketing puesto que es necesario conocer los resultados en tiempo real.
Esto es así porque si algo no está funcionando o no está dando el resultado esperado, se podrá cambiar cuanto antes.

***Relación agencia-anunciante:*** la comunicación es la clave para que esta relación sea lo más fluida y transparente posible.

![](../perfo1.png)

### Brand marketing

El **Brand Marketing o Branding** es la disciplina que se encarga de la creación y gestión de una marca. Cuando se empieza con la gestión o con el branding deberemos dar unos primeros pasos esenciales, por lo que podemos dividir nuestra estrategia de brand marketing en 6 apartados claramente diferenciados:

***Creación.

Identidad corporativa.

Logotipo.

Nombre.

Código de colores.

Tipografía.***

#### Creación
Una marca es una promesa de valor, que debe reflejar tanto la personalidad como los valores y atributos de la firma para tener una esencia única que le permita diferenciarse de las demás.

***Naming*** El primer paso que tenemos que dar cuando empezamos a trabajar en el Branding o Brand Marketing es darle nombre.

Son muchos los nombres de firmas con una gran reconocimiento presentes en el mercado.

Coca Cola, Apple, Tesla, Mercedes, Samsung, Adidas, Dixan, Licor del Polo, Nescafé, Loctite, CAT, Volvo, etc.

#### Identidad corporativa

Esto es, una guía que nos permita gestionar la marca de forma adecuada y estratégica, para que la comunicación o el diseño de todos los elementos relacionados con ella tengan siempre coherencia.

Un buen manual de identidad corporativa que se ajuste a los parámetros del Brand Marketing moderno no puede dejar de incluir una serie de elementos.

#### Logotipo

Para definir el apartado de identidad corporativa de tu marketing branding hazte las siguientes preguntas respecto al logotipo:

*¿Voy a diseñar el logo de un solo color o será de más de uno?*

*¿Dónde voy a colocar el logo en el packaging?*

*¿En qué lugar va el logo en un cartel?*

*¿Cuál será la ubicación del logotipo en nuestra página web?*

*¿Y en nuestro blog?*

*¿Dónde voy a utilizar el logo en mis redes sociales?*

*¿Y en un banner?*

*¿Qué lugar debe ocupar en mis posts para redes sociales?*

*¿El logo debe ir centrado? ¿Arriba y a la derecha? ¿Arriba y a la izquierda? ¿Abajo?*

*¿Sobre qué fondo de color irá el logo o con qué combinación de colores?*

#### Nombre

Debe tener un nombre y en la medida que muchas están asociadas a empresas, es importante encontrar un nombre adecuado y en sintonía con los valores del misma.

El nombre debe además ser original, impactante y fácil de recordar.

Además, y teniendo en cuenta los nuevos parámetros del marketing digital, y en especial el SEO, un nombre para una nueva marca debería ser fácil de posicionar, algo que se obvia en la mayoría de casos y que dificulta mucho las posteriores gestiones de los SEOs par posicionarla en buscadores.

#### Código de colores

La relación entre color y marca es clave en marketing branding al ser el color un componente fundamental de la personalidad propia de la firma.

Por ello, es fundamental definir un color principal que la identifique y una serie de colores secundarios que nos permitan dar salidas creativas a nuestra comunicación corporativa.

#### Tipografía

Escoger lo diferentes tipos de fuentes es fundamental para mantener una comunicación que sea tanto corporativa como visual. En marketing branding, es muy importante definir cuántos tipos de fuente vamos a utilizar y en qué casos vamos a usar cada una de ellas, sin dejar de lado la combinación de colores y su alineación con los diferentes fondos así como su relación con el logo.


###Consigna

"Realizar entrega propuesta por los docentes del taller de emprendimiento.
"
Realizar un listado de herramientas vistas en el taller de emp. que consideres útiles para tu proyecto final.

##Sesión cinco - Dia cinco

### KPI'S

El término **KPI**, siglas en inglés, de **Key Performance Indicator**, cuyo significado en español vendría a ser **Indicador Clave de Desempeño o Medidor de Desempeño**, hace referencia a una serie de métricas que se utilizan para sintetizar la información sobre la eficacia y productividad de las acciones que se lleven a cabo en un negocio con el fin de poder tomar decisiones y determinar aquellas que han sido más efectivas a la hora de cumplir con los objetivos marcados en un proceso o proyecto concreto.

Los KPI son utilizados por diversas ventajas:

. Permiten obtener información valiosa y útil.

. Medir determinadas variables y resultados a partir de dicha información.

. Analizar la información y efectos de unas determinadas estrategias (así como las tareas que se utilizaron para llevar a cabo las mismas).

. Comparar la información y determinar las estrategias y tareas efectivas.

. Tomar las decisiones oportunas.

##Reflexiones

Se tuvo uns sesión de socialización con los compañeros de clase, donde se realizaron actividades como el **MINDFULLNESS**, la cual fue sumamente satisfactoria, además de otras actividades de recreación que invitó al esparcimiento.

## Tarea 1

###BMC de proyecto innovador

![](../images/01.png)

###GANTT de proyecto innovador

![](../images/gantt.JPG)
 **[GANTT DE PROYECTO INNOVADOR](https://docs.google.com/spreadsheets/d/1prmFX0QYu3dYqCI-DPzJ91UaMbRGyQ6rQYT9j_4NWHA/edit?usp=drive_web&ouid=104947867906691281943)**
