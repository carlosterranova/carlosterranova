---

---
#**Mi** Diseño computacional

![](../images/0.4.JPG)

##Duración del módulo: 4 sesiones

## ¿Que es el diseño computacional?

El diseño computacional es uno de los grandes **protagonistas de la transformación digital** en la arquitectura. Las prácticas de diseño computacional se basan en la construcción de redes compuestas de mentes creativas y computadoras.

El diseño computacional es la aplicación de estrategias computacionales para el proceso de diseño. Mientras que los diseñadores tradicionalmente se basan en la intuición y la experiencia para resolver problemas de diseño, el diseño computacional tiene como objetivo mejorar ese proceso mediante la codificación de las decisiones de diseño utilizando un lenguaje de programación. ***El objetivo no es necesariamente documentar el resultado final, sino más bien los pasos necesarios para crear ese resultado.***


## El Docente: Arturo De La Fuente

Arquitecto por la Universidad de Buenos Aires donde actualmente es
profesor de Diseño Parametrico en la maestría de Diseño Interactivo (MAEDI); además es
profesor adjunto en Estructuras III , profesor titular en Tectónica Digital y profesor de Software
en el Programa en Arquitectura y Tecnología en la Escuela de Arquitectura y Estudios Urbanos
Universidad Torcuato Ditella.
Desde hace 4 años es parte del equipo en Damtsa empresa liderada por Dante A. Martinez Tisi
orientada a la fabricación customizada de estructuras metálicas especiales donde realiza el
diseño de ingeniería y fabricación de piezas para obras de arquitectura y arte, en la actualidad
colabora en el proyecto para la ampliación del MoMa NYC.
Es consultor independiente e instructor de Diseño computacional y fabricación digital, dicta
workshops en Buenos Aires y realizó talleres en Rosario, Córdoba, Corrientes, Mendoza y
países como México, Paraguay y Uruguay.

[Arturo De La Fuente](https://www.arturodelafuente.com/)

##*¿Que vimos durante las sesiones?*
Básicamente trabajaremos con el software **Rhinoceros**, mas especificamente con su extensión **Grasshopper**. No obstante, esto se encuentra enmarcado dentro de otras clasificaciones del diseño.

-Diseño algoritmico
-Diseño generativo
-Diseño Asociativo
-Diseño responsivo
-Diseño recursivo - iteractivo
-Diseño genético
-Diseño evolutivo
-Diseño emergente
-Diseño retroalimentativo
-Diseño inteligente
-Diseño form fiding
-Diseño computacional
-Diseño eficiente

![](../images/2.0.jfif)

Vimos algunos ejemplos de como podrian ser la aplicación de estos conceptos:

![](../images/2.3.jfif)

##Introduciendonos al software

Particularmente, siempre le he tenido un respeto grande al software Rhino, puesto que su interfaz no me resultaba muy atractiva visualmente. Esto me llevó a optar por otros programas como Inventor o Fusion 360, siendo esta la oportunidad de poder quitarme esos anticuerpos.

Habia escuchado de Grasshopper, pero nunca habia tenido la oportunidad de poder interactuar con  el programa pero a primera vista, se presenta como un software con multiples funcionalidades y que amerita mucho estudio.

Asi fue que en la primera sesión se realizaron varios ejercicios para poder ir familiarizandose con el mismo.

![](../images/2.4.jfif)

![](../images/2.5.jfif)

![](../images/2.6.jfif)

[Ejercicios realizados en clase](https://drive.google.com/drive/u/0/folders/1Z2C5Y7Tajxy7ty82vhwcdFajmUFuW0-7)

###Recursos

[Swarm intelligence](https://www.youtube.com/watch?v=LHgVR0lzFJc&t=746s)

[Wallacei](https://www.food4rhino.com/en/app/wallacei)

[Weaverbird](https://www.giuliopiacentino.com/weaverbird/)

[Wasp](https://www.food4rhino.com/en/app/wasp)

[Ameba](https://www.food4rhino.com/en/app/ameba)

[Packrat](https://www.food4rhino.com/en/app/packrat)

[Parametric house](https://parametrichouse.com/)

[Bison](https://www.food4rhino.com/en/app/bison)

***Nota mental***

-Rhino puede trabajar tanto con NURBS como con MESH

-Se pueden **descargar pluggins** gratis como pagos desde [Food4rhino](https://www.food4rhino.com/en)

-**Las coordenadas U - V** son las coordenas vinculadas a la geometria local.

##Consigna

"Realizar entrega propuesta por el docente del taller.
Elegir un ejercicio relacionado a tu proyecto final para desarrollar con grasshopper y documentar.
Opcional: Elegir uno de los ejercicios realizados en clase, editarlo y documentar.
Consultas: ***arturo@arturodelafuente.com "***


###Ejercicio 1

![](../images/grass1.JPG)
![](../images/grass2.JPG)
![](../images/gras0.jfif)
![](../images/grass3.JPG)
![](../images/grass4.JPG)

[Ejercicio 1](https://drive.google.com/drive/u/0/folders/1AfdJ6RdPqaGNxeqx-ah77iQBGBWhlePo)

###Ejercicio 2

![](../images/grass5.JPG)
![](../images/grass6.JPG)
![](../images/grass7.JPG)
![](../images/grass8.JPG)
![](../images/grass9.JPG)

[Ejercicio 2](https://drive.google.com/drive/u/0/folders/1sfHZKvOPYpy-q8Zn__05u9Rn0vzPRuDY)

###Ejercicio 3 - Base para trabajar para mi proyecto final

![](../images/grass10.JPG)
![](../images/grass12.JPG)
![](../images/grass13.JPG)
![](../images/grass14.JPG)
![](../images/grass15.JPG)
![](../images/grass16.JPG)
![](../images/grass17.JPG)
![](../images/grass18.JPG)

[Ejercicio 3](https://drive.google.com/drive/u/0/folders/1B_jGxfxF36uayVQV3WuvBleLOb3n6qDK)



***Nota mental 2***
consultas:
*¿como puedo desplazar objetos en rhino con el Gizsmo?*

Se tiene que activar la opción **Gumbal** en la zona inferior de la interface de Rhino.
![](../images/sg1.JPG)
![](../images/gn2.JPG)

Para poder terminar con mi ejercicio 3, necesitaba instalar el pluggin : Weaverbird , sin embargo el software me impedia poder realizar la instlación.
![](../images/grasserror.JPG)

Durante la sesión con Arturo me pudo explicar los siguientes pasos para resolver el problema:

1.Ingresar a la web del creador del Pluggin [Weaverbird](https://www.giuliopiacentino.com/weaverbird/).

 ![](../images/0.01.JPG)

2.Descargar la opción alternativa en la misma página del creador y posteriormente ejecutar el Software como Administrador.

 ![](../images/0.002.JPG)

 3.Una vez realizada esta acción, ya pude instalar el pluggin para poder seguir con las recomendaciones de Arturo.

 ![](../images/0.003.JPG)

###Nota recordatorio
La salución se encuentra en el minuto 1:18:59 hasta 1:35:45 del video de sesión del 10/12/2021.


[![](../images/0.02.JPG)](https://drive.google.com/drive/folders/186b49HyPxb5iN2IwUqW-VhoeUHqtUCh9)


 .
