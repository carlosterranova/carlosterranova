#**MT 10** Molding and casting

![](../images/mc1.png)

### CONCEPTO

El moldeado o fabricación de moldes es el acto de crear la cavidad / forma que lleva una impresión negativa o inversa de un modelo original. Los moldes pueden estar hechos de un material rígido, como yeso o resina plástica o, más comúnmente, de un material flexible como el caucho. El material a utilizar debe elegirse teniendo en cuenta el material del modelo, el material que se utilizará para hacer las coladas y si hay socavaduras.

La fundición es el acto de verter material líquido en la cavidad de un molde. Después de un período de tiempo, este líquido se curará mediante reacción química o enfriamiento. La parte solidificada también se conoce como fundición, que se expulsa o se rompe fuera del molde para completar el proceso. Los materiales de fundición suelen ser metales o varios materiales de fraguado en frío que curan después de mezclar dos o más componentes; ejemplos son epoxi, hormigón, yeso y arcilla.



###¿CON QUE MATERIALES PODEMOS TRABAJAR?

MATERIALES PARA LA FABRICACIÓN DE:
Madera
Metal
Silicona
Arena
Plástico
Yeso

PIEZAS FABRICADAS CON:

Moldes de Silicona para coladas
Colada de Material (resina, yeso , cemento)
Laminado de materiales compuestos (fibra de vidrio, carbono, kevlar)
Termoformado.(plásticos)
Cera Perdida (fundición de metales)


### DESAFÍO

Diseñar un modelo de maceta ( dimensiones: no exceder los 10 cms de alto por 15 cms de diámetro)

### PROCESO

Modelado en Fusion 360

![](../images/mc2.png)

![](../images/mc3.png)

![](../images/mc4.png)

![](../images/mc5.png)

![](../images/mc6.png)

![](../images/mc7.png)

![](../images/mc8.png)

### ARCHIVO FUSION 360
[ARCHIVO FUSION](https://drive.google.com/file/d/1nCOsNxjKZDPb_NaICPlGorr2EsQye58x/view?usp=sharing)

### ARCHIVO STL
[ARCHIVO STL](https://drive.google.com/file/d/1v84a4kCxA34MCDwC_0HKMKnddWOzDtSt/view?usp=sharing)
