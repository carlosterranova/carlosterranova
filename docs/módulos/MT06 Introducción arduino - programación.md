#**MT 06** Introducción arduino - programación

![](../images/ardui.JPG)

### INFORMÁTICA FÍSICA
La informática física o computación física puede ser definida como la rama del conocimiento que se encarga del estudio de la relación entre los ordenadores (mundo virtual) con la realidad física (mundo físico) que nos rodea. Esta disciplina surge ante la necesidad de acercar estos dos mundo que, tradicionalmente, chocaban frontalmente.
En un mundo actual en los que somos capaces desde nuestros ordenadores o dispositivos móviles de realizar acciones sobre el medio físico que nos rodea (Internet de las cosas) es necesario crear una serie de herramientas que nos permitan derribar ese muro e interconectar los "mundos reales y virtuales".

![](../images/00001.JPG)
[INFORMÁTICA FÍSICA](https://vimeo.com/153278841?embedded=true&source=vimeo_logo&owner=13741493)

### ARDUINO

Arduino es una plataforma de creación de electrónica de código abierto, la cual está basada en hardware y software libre, flexible y fácil de utilizar para los creadores y desarrolladores. Esta plataforma permite crear diferentes tipos de microordenadores de una sola placa a los que la comunidad de creadores puede darles diferentes tipos de uso.

#### ¿HARDWARE LIBRE?

El hardware libre son los dispositivos cuyas especificaciones y diagramas son de acceso público, de manera que cualquiera puede replicarlos. Esto quiere decir que Arduino ofrece las bases para que cualquier otra persona o empresa pueda crear sus propias placas, pudiendo ser diferentes entre ellas pero igualmente funcionales al partir de la misma base.

El software libre son los programas informáticos cuyo código es accesible por cualquiera para que quien quiera pueda utilizarlo y modificarlo. Arduino ofrece la plataforma Arduino IDE (Entorno de Desarrollo Integrado), que es un entorno de programación con el que cualquiera puede crear aplicaciones para las placas Arduino, de manera que se les puede dar todo tipo de utilidades.

#### NACIMIENTO DE ARDUINO

El proyecto nació en 2003, cuando varios estudiantes del Instituto de Diseño Interactivo de Ivrea, Italia, con el fin de facilitar el acceso y uso de la electrónico y programación. Lo hicieron para que los estudiantes de electrónica tuviesen una alternativa más económica a las populares BASIC Stamp, unas placas que por aquel entonces valían más de cien dólares, y que no todos se podían permitir.

El resultado fue Arduino, una placa con todos los elementos necesarios para conectar periféricos a las entradas y salidas de un microcontrolador, y que puede ser programada tanto en Windows como macOS y GNU/Linux. Un proyecto que promueve la filosofía 'learning by doing', que viene a querer decir que la mejor manera de aprender es cacharreando.

### ¿COMO FUNCIONA ARDUINO?

El Arduino es una placa basada en un microcontrolador ATMEL. Los microcontroladores son circuitos integrados en los que se pueden grabar instrucciones, las cuales las escribes con el lenguaje de programación que puedes utilizar en el entorno Arduino IDE. Estas instrucciones permiten crear programas que interactúan con los circuitos de la placa.

El microcontrolador de Arduino posee lo que se llama una interfaz de entrada, que es una conexión en la que podemos conectar en la placa diferentes tipos de periféricos. La información de estos periféricos que conectes se trasladará al microcontrolador, el cual se encargará de procesar los datos que le lleguen a través de ellos.

El tipo de periféricos que puedas utilizar para enviar datos al microcontrolador depende en gran medida de qué uso le estés pensando dar. Pueden ser cámaras para obtener imágenes, teclados para introducir datos, o diferentes tipos de sensores.

![](../images/A1.JPG)
[COMO FUNCIONA ARDUINO](https://www.youtube.com/watch?v=Zs9MZosVuqo)

### MUNDO Arduino

![](../images/A2.JPG)
[MUNDO ARDUINO](https://www.arduino.cc/)

![](../images/A3.JPG)

### CONSIGNA

"MT06 - Desafío
LEER MINIMO DOS INPUTS
 ( recomendamos Sensor de Temperatura + Fotoresistencias)
ACTUAR MINÍMO DOS OUTPUTS
( recomendamos sermotor + motor dc con transistor)
NOTA: Puntos extra por similar el circuito tambien en Thinkercad Electronics
Documentar tu proceso y publicar en tu repositorio de gitlab.
"
### VÍNCULO CON PROYECTO FINAL

Se realizaron pruebas técnicas para la futura inclusión de la programación dentro del proyecto final, por lo que se utilizó:

1. [Módulo lector microsd (MINI MP3 PLAYER)](https://www.electroallweb.com/index.php/2020/07/22/modulo-dfplayer-mini-reproductor-mp3-tutorial-completo/)
2. 3 LEDs amarillo , rojo y verde
3. Sensor de proximidad HC-SR04
4. Arduino uno
5. Resitencias de 1/8 watt

![](../images/ardui3.png)
Se descargaron bibliotecas de internet para poder hacer trabajar los distintos componentes de la programación dentro del circuito propuesto para el diseño del proyecto final.

![](../images/ardui4.png)
Se procedió a modificar las variables para se ajuste a los estímulos que se han planteado (PROXIMIDAD) para que se active el circuito total del diseño.

![](../images/ardui5.png)
Además se setteo a las luces leds para que sirvan de indicadores tanto para inicio del circuito como para su estado de culminación y pausa.

Finalmente se realizaron las pruebas de rigor para probar su correcto funcionamiento.

[EVIDENCIA UNO](https://photos.app.goo.gl/LYZKTmVeqZNnK8U36)
[EVIDENCIA DOS](https://photos.app.goo.gl/imxSiXyKcFzMT3P4A)

Una vez finalizada las pruebas técnicas, se procedió a realizar su inserción dentro del case del diseño final.

![](../images/ardui6.jpg)

![](../images/ardui7.jpg)

![](../images/ardui8.jpg)

![](../images/ardui9.jpg)

![](../images/ardui10.jpg)

###PROYECTO FINAL
Para mayor información del funcionamiento total del proyecto final, revisar su respectivo apartado en el siguiente [link](https://carlosterranova.gitlab.io/carlosterranova/Proyecto%20juguete%20tecnol%C3%B3gico/proyecto/)
