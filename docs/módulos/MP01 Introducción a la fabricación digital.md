---

---
#**MP 01** Introducción a la fabricación digital e innovación abierta

![](../images/01.2.JPG)

##*¿Que me motivó para hacer la EFDIA?*

Estudié diseño industrial en la Pontificia Universidad Católica del Perú. Siempre he estado involucrado en el desarrollo de prototipos de productos utilizando tanto programación como fabricación digital, sin embargo, no llegué a profundizar en en los procesos productivos o la ciencia que se encuentra detrás de los mismos. Por ello, me resultaba necesario poder formarme en las disciplinas de fabricación digital  para tener mayor autonomía a la hora de desarrollar un nuevo proyecto.

##*¿Que se verá durante la especialización?*

Se derrollarán disciplinas como:

Emprendimiento
Diseño web
Diseño 2D para láser y CNC
Corte láser
Prototipado
Modelo 3D
Escaneado 3D para prototipado rápido
Arduino y programación
etc.

![](../images/Intro2.JPG)

##*Plan de estudios*
![](../images/plan1.jpg)
![](../images/plan2.jpg)
![](../images/plan3.jpg)

[PDF PLAN DE ESTUDIOS](https://drive.google.com/drive/u/0/folders/1-5LZgTq8Zqrt1aAKyVQeYoV03dhj4obF)

##*¿En que nivel de conocimiento me encuentro actualmente?*

Durante este proceso, se buscó identificar el nivel de familiarización con las disciplinas a tratar dentro de la especialización. Por mi carrera me identifico mas con los softwares de modelado 3D y corte láser, siendo el gran reto personal poder aprender a programar en arduino y diseño paramétrico.

Por ello se creo este cuadro para mapear los skills.



![](../images/level.JPG)

##*¿Cuales serán las plataformas de aprendizaje?*

![](../images/mp1.4.JPG)



# DESAFIO

"Acceso a discord y presentación, revisión de info EFDIA. Posible idea para proyecto final.
"
![](../images/fda.JPG)
