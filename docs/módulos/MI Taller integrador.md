#**TALLER INTEGRADOR**

![](../images/ti.png)

### Calendario

**Dia UNO (Miercoles 30/3):**

1 - Formar equipos de trabajo (estudiantes presenciales y online)
para realizar las siguientes tareas:

● Montar una extrusor de pasta partiendo de una Anet A8

● Programar la placa de la Anet A8 con Arduino IDE

● Modelar e imprimir algo en 3d con materiales biodegradables

● Realizar una presentación del trabajo realizado en los tres días

2 - Soldado de placas del módulo MT08
Documentar en equipo el proceso y publicar en tu repositorio de

**Dia DOS(Jueves 31/3):**

Armado de equipos de trabajo (3) / división detareas https://miro.com/app/board/o9J_lxIOIrc=/

● Preparación Marlin Anet A8 / programación

● Montaje extrusor de pasta

● Pruebas de impresión en pasta

● Impresión 3d fdm (piezas para mejorar extrusor)

**Dia Tres(Viernes 1/4):**

Montaje de piezas impresas en fdm

● Preparación de pasta (probar tres opciones)

● Modelado 3d de pieza a imprimir en pasta

● Experimentación

[PDF](https://drive.google.com/file/d/1vg0jpC-10aAaR-b6czP1jdTzx-77M3Oc/view?usp=sharing)

### Concepto del evento

Se buscó montar y realizar pruebas técnicas con una impresora que pueda extruir diferentes tipos de materiales.
Se intervino una impresora ANET A8 a la cual se le debía adaptar el dispositivo para la extrusión para la pasta, además de utilizar el software de slicer.
gitlab.


###VIDEOS DEL PROCESO

![](../images/ti1.png)
[video 1](https://drive.google.com/file/d/1eP7alSoMO805Bx3EAtql57rm4Ps8WtLg/view?usp=sharing)

![](../images/ti2.png)
[video 2](https://drive.google.com/file/d/1mzpuM3PGaZkGbG7e3fRvlCmyN12cgPoI/view?usp=sharing)

![](../images/ti3.png)
[video 3](https://drive.google.com/file/d/1HCq2V13Cp3cCtTEKdbbmb-5WrPdUWon2/view?usp=sharing)

![](../images/ti4.png)
[video 4](https://drive.google.com/file/d/1DrNPaQYiRODYDe_RBpzSwkuXPxuWLNeZ/view?usp=sharing)

###Observación

Mi participación se dió como soporte remoto en el armadon de la presentación final del equipo.
