---

---
#**MP 02** Prototipado

![](../images/0.5.JPG)

Este módulo consta de cuatro jornadas de trabajo en el cual se buscaba conocer herramientas para la definición, construcción y escalamiento de los proyectos en los cuales nos encontramos involucrados.

## 1. Futuros emergentes

### Atlas de futuros emergentes

Docente: Mariana Quintero

Utilizamos la herramienta de Atlas de futuros emergentes para poder añadirle mayor valor a nuestra propuesta  desde la etapa inicial a partir de escenarios, conceptos y metodologias que son o serán las de más relevancia en el mundo del futuro.

Este esquema funciona por medio de :
-Señales débiles
-Conceptos detonantes
-Áreas de oportunidad.

![](../images/0.3.JPG)

[Regenerative Bussines Model  Ejemplos](https://now.partners/es/creacion-del-valor-regenerativo/)

[Circuar canvas](https://circulab.com/es/toolbox-circular-economy/circular-canvas-regenerative-business-models/)

[Miro del ejercicio](https://miro.com/app/board/o9J_lquo98o=/)

## 2. Como comunicar tu proyecto
Docentes: Marcel Rodriguez

En esta sesión se brindaron los elementos clave para  definir, comprender el público objetivo de nuestro  proyecto.

Las relaciones clave para el éxito de nuestro proyectos, es el entendimiento de nuestros usuarios para con ello, diseñar de forma correcta, ademásn de establecer canales de comunicación cercana.

Es asi como realizamos un ejercicio base para definir en pocas palabras (Tweet) la definición de nuestro proyecto.

![](../images/001.JPG)

Asi mismo, utilizamos la herramienta de Moodboard para poder definir el "look and feel" de nuestro proyecto, además de poder tener siempre a la mano los referentes que estamos utilizando para la diferentes etapas del proyecto.

![](../images/002.JPG)

## 3. Diseño Multiescalar

Docente: Mariana Quintero

Este sesión es una continuación  del el taller del día lunes.

Se busca conectar las cartas presentadas con  algún proyecto  o experiencia desarrollada anteriormente y utilizar estas  en un Mapeo multiescalar.

La intención es encontrar conexiones y/o referencias para encontrar referentes, a diferentes escalas

![](../images/004.JPG)


## 4. Project Managment

Docente: Santiago Fuentemilla

Dia final del módulo

Se centró en  la herramienta “backcasting”.

Se buscó plantear la posición de tener la idea enteramente atada  con el objetivo final y de marcar las actividades  clave dentro del proyecto.

 La intención es poder añadir la mayor cantidad de detalles, para generar claridad y especificación a todo nivel dentro del trabajo.

#### Video de Nel Gerchenfield

[![](../images/005.JPG)](https://vimeo.com/558297880)

## Aplicaciónes a tomar en consideración.


[IFTTT](https://ifttt.com/onboarding)

[Lightburn](https://lightburnsoftware.com/)

[Futuros emergentes](https://community.emergentfutures.io/)

[Blockchain](https://ajuntament.barcelona.cat/digital/es/transformacion-digital/city-data-commons/blockchain-para-la-soberania-de-los-datos-decode)

[ELISAVA](https://www.elisava.net/es/noticias/elisava-insights-un-informe-de-elisava-research)
