
#**MT 03** Láser controlado por computadora

![](../images/0.2.JPG)

El corte láser es controlado por computadora lo que nos permite obtener un producto de una manera rápida, precisa y de alta repetibilidad para crear piezas de todas las formas y tamaños con un acabado perfecto.

El objetivo de este módulo es demostrar y describir procesos de diseño 2D paramétrico y de esta forma involucrar los procesos del uso de la cortadora láser.

De esta forma, se pudo adquirir el conocimiento como diferenciar procesos como  grabado de raster, corte y delineado sobre vector.


##Tipos de corte láser

**Corte por sublimación láser:** La alta intensidad del haz laser vaporiza el material directamente en el punto de trabajo. Luego, por lo general se usa un gas inerte para cortar, o sea, para expulsar el material y generar la ranura de corte. Esta situación la encontramos principalmente restringida al corte de sustancias no metálicas, como ser madera, papel, cerámica o plástico.

**Corte por fusión láser:** Aquí, el material fundido o derretido por el haz laser es expulsado por medio de nitrógeno, generando la ranura de corte. El nitrógeno es inyectado en la boquilla a alta presión (hasta 20 bar) y al salir de ella por una pequeña perforación de la punta, se convierte en un chorro de alta energía cinética. La fusión laser es particularmente utilizada en el corte de aceros cromo-níquel o aluminio libre de óxido, produciendo superficies de corte metalúrgicamente limpias.

**Corte por quemado láser:** El haz de láser calienta el material hasta su temperatura de encendido. Aquí se usa oxígeno como gas de corte. Después de alcanzada la temperatura de encendido, el material “se quema” con un chorro de oxígeno, generando una reacción exotérmica. La escoria producida es expulsada por medio del gas de corte generando la ranura de corte. Este proceso es particularmente utilizado para el corte de aceros dulces y en menor escala para cortes de acero inoxidable.


### Raster vs Vector en el corte láser

La principal diferencia entre un grabado de trama y uno de corte vectorial, es que para el
grabado de la cabeza del láser que va izquierda a derecha en el área de impresión y luego
se mueve hacia abajo y repite hasta que se ha grabado la imagen. Con el vector cortado el
láser simplemente traza las líneas de corte. Como resultado, el grabado de trama tarda
mucho más tiempo que los recortes del vector.
Entonces, si tenemos un diseño como un mapa, que en su mayoría son líneas. ¿Se puede
ejecutar como un grabado de trama?

La ventaja de esto es que se puede establecer la línea de espesor a lo que desees y que
cuentas con diferentes líneas con diferentes grosores. La desventaja es que va a tomar
mucho más tiempo para grabar.
Si el diseño es un archivo vectorial (esto no funciona con imágenes de mapa de bits) hay
una manera más rápida para crear sus líneas. Establecer el archivo como un vector de corte
pero a su vez baja la potencia y aumenta la velocidad. Por ejemplo, para cortar a través de
1/8 “de madera contrachapada pondríamos la potencia del láser al 100% y la velocidad a
20%, pero justo al acabar de marcar la madera cambiaríamos la potencia al 30% y la
velocidad a 95%. Así que en lugar de cortar a través del material del láser quema
simplemente una línea delgada en ella. La ventaja es que va a ser mucho más rápido que
el grabado. La desventaja es que la línea va a quedar muy delgada y no se podrá variar el
espesor de la misma.



###Nota importante
Exísten códigos de color que te permiten identificar y sistematizar los procesos como:

ROJO : Corte sobre vector
AZUL:  Delineado sobre vector
NEGRO: Grabado raster.

##Tolerancias

Dependiendo del espesor y del material, la precisión del corte láser puede ser muy
rigurosa, llegando a +/- 0,1 mm. Lo que hace el Corte Láser no solo un servicio
preciso, sino también económico. Requerir mayor precisión cuando no es necesaria
puede encarecer el servicio inútilmente.
La tolerancia estándar ronda los +/- 0,2 mm en los bordes, y para los entre centros
es de +/- 0,05 mm para piezas que no superen 500 x 500 mm.
Al solicitar la cotización es conveniente especificar la tolerancia requerida.

##Kerf bending

Es una técnica tradicional, que puede ser aplicada para el corte láser y en fresadora CNC.
Para lograr un curvado gracias a patrones de corte, lo primero que se hace es hacer cortes
en la cara interna del doblez, el lado "no expuesto". El ancho, la profundidad del corte, el
número y la separación entre líneas, son variables que afectan la calidad y la terminación
del doblez.

##RDWORKS

Este software tiene soporte para dibujar puntos, líneas, polilínea, elipse / círculo, rectangular / cuadrado, curva Bezier y texto.

Soporta los siguientes formatos:
Dxf, ai, plt, dst, dsb y formatos relacionados.
Formato de mapa de bits: bmp, jpg, gif, png, mng y formatos relacionados.
La aplicación se puede ejecutar en los siguientes sistemas operativos: Windows XP, Windows 7,Windows 8 / 8.1 y Windows 10. Además, se recomienda que tu PC esté equipada con un procesador de doble núcleo y que tenga al menos 2 gb de RAM instalada.

![](../images/RD.JPG)

SOFTWARES A EXPLORAR  

**[RDWORKS](https://drive.google.com/drive/folders/19QL0qIKQ8fpWgQItXSUGu5J7pvzF_9-D)**

**[GIMP](https://www.gimp.org/)**

**[Krita](https://krita.org/en/)**

**[Pixlr](https://pixlr.com/)**

**[Inkscape](https://inkscape.org/)**

**[Fusion 360](https://latinoamerica.autodesk.com/products/fusion-360/overview?mktvar002=4424517|SEM|13037407432|120424532325|aud-1166343329429:kwd-11029869505&ef_id=CjwKCAjwk6-LBhBZEiwAOUUDp_BML0e1_UnQlbYUtQLvXy_hMctBaH5OipX3o8PCLDEdMx5nPfDCKxoCimcQAvD_BwE:G:s&s_kwcid=AL!11172!3!549334725048!e!!g!!fusion%20360!13037407432!120424532325&mkwid=s|pcrid|549334725048|pkw|fusion%20360|pmt|e|pdv|c|slid||pgrid|120424532325|ptaid|aud-1166343329429:kwd-11029869505|pid|&utm_medium=cpc&utm_source=google&utm_campaign=GGL_DEC_AutoCAD_AMER_MX_eComm_SEM_BR_New_EX_ADSK_3360535_General&utm_term=fusion%20360&utm_content=s|pcrid|549334725048|pkw|fusion%20360|pmt|e|pdv|c|slid||pgrid|120424532325|ptaid|aud-1166343329429:kwd-11029869505|&gclid=CjwKCAjwk6-LBhBZEiwAOUUDp_BML0e1_UnQlbYUtQLvXy_hMctBaH5OipX3o8PCLDEdMx5nPfDCKxoCimcQAvD_BwE)**

**[Illustrator](https://www.adobe.com/la/products/illustrator.html?sdid=KQPQL&mv=search&ef_id=CjwKCAjwn8SLBhAyEiwAHNTJbU_iyuN5znV4d9k7FUdf0W7tfQLbrsiUJGH7eJZonIq53uIgZZcfPRoCby4QAvD_BwE:G:s&s_kwcid=AL!3085!3!442303212642!e!!g!!illustrator!9499870682!97813414318&gclid=CjwKCAjwn8SLBhAyEiwAHNTJbU_iyuN5znV4d9k7FUdf0W7tfQLbrsiUJGH7eJZonIq53uIgZZcfPRoCby4QAvD_BwE)**

**[Slacer for Fusion](https://knowledge.autodesk.com/es/support/fusion-360/troubleshooting/caas/downloads/downloads/ESP/content/slicer-for-fusion-360.html)**

CONSIGNA DE MT03

"Diseñar un objeto para ser fabricado en máquina láser.
Material a utilizar: MDF de 3mm de espesor. El objeto debe tener como mínimo 3 piezas que lo compongan.  Las piezas se deben de poder ensamblar mediante encastres (sin la utilización de pegamento o fijaciones externas). El objeto a fabricar, debe de contener  las 3 operaciones básicas de la máquina láser (grabado raster, marcado sobre vector y corte sobre vector). El objeto a diseñar debe aplicar en alguna de sus partes, la técnica de curvado de madera (kerf bending). Tener en cuenta que la cantidad total de material a utilizar por estudiante será de 600 x 450 mm. Se cortarán las piezas diseñadas en un sólo archivo (formato .dxf) que entren dentro de esas dimensiones de material. Documentar el proceso y publicarlo en el respositorio Gitlab."





PROCESO

Ante el reto impuesto, busqué un archivo que habia trabajado hace algunos años donde se buscaba crear una caja musical en forma del Volkswagen Kombi, el cual vinculaba tambien elementos electrónicos como pulsadores o un ardunio NANO. Sin embargo, el prototipo falló a la hora de generar los dobleces (Kerf bending) por lo que consideré una oportunidad para generar una versión mejorarada.

Paso 1:

Ubicar el archivo original, entender la geometria e identificar errores.

![](../images/mt03.0.JPG)

Paso 2:
Adaptar el diseño de 2.5 mm de espesor a 3 mm.

![](../images/mt03.1.JPG)

Paso 3:
Aplicación del Kerf Bending aprendido en clase.
![](../images/mt03.2.JPG)

Paso 4:
Preparación del archivo de corte para pruebas durante clase.
![](../images/mt03.3.JPG)

Paso 5:
Seteo de RDWorks

### Archivos de corte

**[Formatos de corte](https://drive.google.com/drive/folders/1EmD_SxwAQVxgwdXwY7bz-bfWF2WNQ5Am)**


#### Nota importante

Envié mis archivos pero por problemas de conectividad, el correo llegó tarde para su revisión y corte dentro de los laboratorios de UTEC.
