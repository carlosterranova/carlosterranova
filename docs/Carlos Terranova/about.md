# Sobre mi

![](../images/foto-perfil.jpg)

Mi nombre es Carlos Alfredo Terranova Véliz (Terra), Diseñador industrial PUCP, maker de vocación y explorador constante de la versatilidad del diseño en los diferentes ámbitos de la vida cotidiana.
Nací en Chiclayo – Lambayeque y viví por mucho tiempo en la ciudad de Tumbes, teniendo muy presente su cultura e historia, la cual busco vincular en los proyectos a los cuales me involucro.
Mi formación como diseñador me ha permitido entender y ser partícipe de los procesos que competen un proyecto creativo y que van desde la conceptualización y desarrollo, hasta la gestión y búsqueda de aliados estratégicos para éxito del mismo.

Junto a grandes colegas y amigos, tuve la oportunidad de ganar premios internacionales ligados a la Robótica social, vehículos de propulsión humana para la Nasa o emprendimiento social. Tambien formo parte de la comunidad Global Shaper – Lima Hub, iniciativa impulsada por el World Ecomic Forum que busca reunir a diferentes personas que están generando impacto positivo dentro de su comunidad y aportar de manera colaborativa sus esfuerzos

Actualmente trabajo en el ministerio de educación del Perú MINEDU en el diseño del proyecto: Centros comunitarios digitales, que busca llevar el pensamiento STEAM y la fabricación digital a todo el pais. **[my website](https://www.backupmakers.com/)**
